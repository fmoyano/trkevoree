package kevoree.trustAPI;

/**
 * Class that represents information about trust factors.
 * @param <F>   type of the factor value.
 */
final class FactorInfo<F> {

    /**
     *  Context of the factor.
     */
    private String context;

    /**
     * Entity that creates/changes the factor.
     */
    private String source;

    /**
     * Name of the factor.
     */
    private String name;

    /**
     * Value of the factor.
     */
    private F value;

    /**
     * Initial value of the factor.
     */
    private F initialValue;

    /**
     * Entity to which the factor refers.
     */
    private String target;

    /**
     * Time stamp of the factor.
     */
    private long timeStamp;

    /**
     * Constructor.
     * @param ctx   the context.
     * @param src   source entity.
     * @param nm    name.
     * @param val   value.
     * @param iVal  initial value.
     * @param tar   target entity.
     * @param ts    time stamp.
     */
    FactorInfo( String ctx, String src, String nm, F val, F iVal, String tar, long ts )
    {
        context = ctx;
        source = src;
        name = nm;
        value = val;
        initialValue = iVal;
        target = tar;
        timeStamp = ts;
    }

    /**
     * Getter of the context.
     * @return the context.
     */
    String getContext()
    {
        return context;
    }

    /**
     * Getter of the entity that created the factor.
     * @return  the source entity.
     */
    String getSource()
    {
        return source;
    }

    /**
     * Getter of the name of the factor.
     * @return  the name.
     */
    String getName()
    {
         return name;
    }

    /**
     * Getter of the value.
     * @return  the value.
     */
    F getValue()
    {
        return value;
    }

    /**
     * Getter of the initial value.
     * @return  the initial value.
     */
    F getInitialValue()
    {
        return initialValue;
    }

    /**
     * Getter of the entity to which the factor refers.
     * @return  the target entity.
     */
    String getTarget()
    {
        return target;
    }

    /**
     * Time stamp of the factor.
     * @return  time stamp.
     */
    long getTimeStamp()
    {
        return timeStamp;
    }
}
