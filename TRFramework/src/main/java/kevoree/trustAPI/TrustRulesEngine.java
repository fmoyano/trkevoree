package kevoree.trustAPI;

import kevoree.trCommon.kevReflection.GetHelper;
import kevoree.trCommon.kevReflection.ScriptEngine;
import org.kevoree.ContainerRoot;
import org.kevoree.api.ModelService;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This class represents the engine that manages the reconfiguration rules for trust models. It is an internatl class that
 * mediates between the trust model and the ScriptEngine.
 */
final class TrustRulesEngine {

    /**
     * File reader for the file.
     */
    private FileReader fi;

    /**
     * Path for reading the file.
     */
    private Path path;

    /**
     * ScriptEngine instance.
     */
    private ScriptEngine se;

    /**
     * Constructor.
     * @param fileName  the name of the file with the reconfiguration policies.
     * @param se        the instance of a script engine.
     */
    TrustRulesEngine( String fileName, ScriptEngine se ) {
        try {
            fi = new FileReader( fileName );
            path = Paths.get(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.se = se;
    }

    /**
     * This method execute the rules specified in the reconfiguration policies file.
     * @param model         the root of Kevoree model.
     * @param idSource      the entity that wants to execute the rules (i.e. the trustor).
     * @param idTarget      the target that is to be considered while executing the rules (i.e. the trustee).
     * @param trustValue    the trust value associated to the trust relationship between the trustor and the trustee.
     * @param threshold     the threshold (if any) associated to the trust relationship.
     */
    void executeRules( ModelService model, String idSource, String idTarget, String trustValue, String threshold )
    {
        List<String> rules = new ArrayList<String>();
        try {
            rules = Files.readAllLines(path, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for( String rule : rules )
        {
            try {
                processRule(rule, model.getCurrentModel().getModel(), idSource, idTarget, trustValue, threshold);
            } catch (TrustException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method process a single rule.
     * @param rule          a String containing the rule to be executed.
     * @param model         the root of Kevoree model.
     * @param idTrustor     the entity that wants to execute the rules (i.e. the trustor).
     * @param idTrustee     the target that is to be considered while executing the rules (i.e. the trustee).
     * @param trustValue    the trust value associated to the trust relationship between the trustor and the trustee.
     * @param threshold     the threshold (if any) associated to the trust relationship.
     * @throws TrustException
     */
    private void processRule( String rule, ContainerRoot model, String idTrustor, String idTrustee, String trustValue, String threshold )
        throws TrustException
    {
        StringTokenizer st1 = new StringTokenizer( idTrustor, "@");
        String trustorInstanceName = st1.nextToken();
        String trustorComponentType = GetHelper.getComponentTypeFromInstance(model, trustorInstanceName);

        StringTokenizer st2 = new StringTokenizer( idTrustee, "@");
        String trusteeInstanceName = st2.nextToken();
        String trusteeComponentType = GetHelper.getComponentTypeFromInstance(model, trusteeInstanceName);

        StringTokenizer st = new StringTokenizer( rule );
        String trustorType = st.nextToken();
        String trusteeType = st.nextToken();

        if( trustorType.equals( trustorComponentType ) && trusteeType.equals( trusteeComponentType ))
        {
            String condition = st.nextToken();
            char booleanValue;
            String value;
            if ( condition.equals( "threshold" ) && threshold != null )
            {
                booleanValue = '<';
                value = threshold;
            }
            else if ( condition.equals( "threshold" ) && threshold == null )
            {
                throw new TrustException( "Rule includes threshold, but threshold is null");
            }
            else
            {
                booleanValue = condition.charAt(0);
                value = condition.substring(1);
            }
            if( conditionSatisfied( booleanValue, value, trustValue ))
            {
                String action = st.nextToken();
                List<String> args = new ArrayList<String>();
                while ( st.hasMoreTokens() )
                {
                    args.add( st.nextToken() );
                }

                se.execute( action, idTrustee, args );
            }
        }
    }

    /**
     * This method checks whether a condition is satisfied.
     * @param booleanOperator   the boolean operator of the condition.
     * @param lhs               left-hand side of the operator.
     * @param rhs               right-hand side of the operator.
     * @return                  true if the condition is satisfied; false otherwise.
     */
    private boolean conditionSatisfied( char booleanOperator, String lhs, String rhs )
    {
        float val = Float.parseFloat( lhs );
        float threshold = Float.parseFloat( rhs );
        if( '<' == booleanOperator )
        {
            return val > threshold;
        }
        else if ( '>' == booleanOperator )
        {
            return val < threshold;
        }
        else
        {
            return threshold == val;
        }
    }
}
