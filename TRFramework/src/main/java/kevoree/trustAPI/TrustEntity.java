package kevoree.trustAPI;

import com.eclipsesource.json.JsonArray;
import kevoree.trCommon.kevReflection.GetHelper;
import org.kevoree.annotation.*;
import org.kevoree.api.Callback;
import org.kevoree.api.Context;
import org.kevoree.api.ModelService;
import org.kevoree.api.Port;
import org.kevoree.api.handler.ModelListener;
import org.kevoree.api.handler.UpdateContext;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * This class represents a trust entity, which is an entity that can be part of a trust relationship as a trustor, trustee or both.
 * Trust entities can issue subjective factors about themselves or in relation to other entities as well.
 * @param <T>   type of the trust values associated to the trust relationships of the entity.
 * @param <F>   type of the subjective factors that this entity can issue.
 */
@ComponentType
public class TrustEntity<T, F> implements ModelListener {

    /**
     * Role played by the entity.
     */
    @Param(defaultValue = "both")
    private String role;

    /**
     * Context where the trust relationships will take place.
     */
    @Param(defaultValue = "MyContext")
    private String trustContext;

    /**
     * Group to which the entity belongs.
     */
    @Param(defaultValue = "MyGroup")
    private String group;

    /**
     * Initial value of the trust relationships of this entity.
     */
    @Param(defaultValue = "0")
    private String bootstrappingTrustValue;

    /**
     * Path of the file that specifies the subjective factors that this entity issues.
     */
    @Param
    private String subjectiveFactorsFilePath;

    /**
     * Kevoree context.
     */
    @KevoreeInject
    private Context context;

    /**
     * Access to the Kevoree model.
     */
    @KevoreeInject
    private ModelService model;

    /**
     * Port through which the entity requests a trust relationship update.
     */
    @Output
    private Port requestTrustUpdate;

    /**
     * Port through which the entity initializes its trust relationships.
     */
    @Output
    private Port initTrustRelationships;

    /**
     * Port through which the entity adds a subjective factor.
     */
    @Output
    private Port addFactor;

    /**
     * Unique identifier of the trust entity.
     */
    private String uid;

    /**
     * Cache of trust relationships (trustee, trust value)
     */
    private HashMap<String, T> trustCache;

    /**
     * Initial trustees of the trust entity
     */
    private List<String> trustees;


    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     */
    @Start
    protected void start()
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();
        trustCache = new HashMap();
        model.registerModelListener( this );
        storeSubjectiveFactors();
    }

    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     */
    @Stop
    public void stop() {}

    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     */
    @Update
    public void update()
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();
    }

    /**
     * It returns a String representing the value of a parameter of the entity.
     * @param idComponent   the unique identifier of the component.
     * @param param         the name of the parameter.
     * @return  the value of the parameter.
     */
    protected final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@" ).nextToken();
        System.out.println("Calling getParam with param " + param + " and instance " + instance);
        return GetHelper.getParamFromInstance( model.getCurrentModel().getModel(), instance, param );
    }

    /**
     * This method creates a subjective factor, or modifies one if it already exists.
     * @param name          the name of the factor.
     * @param increment     the increment over the current value of the factor, if it already exists. If set to null, the value
     *                      will be replaced by the next parameter: initialValue.
     * @param initialValue  the initial value of the factor, if it does not exist yet or if the client wants simply to replace it.
     * @param uidTarget     the unique identifier of the entity to which the factor refers.
     */
    protected final void changeSubjectiveFactor( String name, F increment, F initialValue, String uidTarget )
    {
        if( "trustor".equals( role ) || "both".equals( role ))
        {
            FactorInfo<F> fi = new FactorInfo(trustContext, uid, name, increment, initialValue, uidTarget, Calendar.getInstance().getTime().getTime());
            addFactor.send(fi);
        }
    }

    /**
     * This method returns the last computed trust value of the trust relationship with a given entity.
     * @param uidTarget the unique identifier of the trustee.
     * @return  the last computed trust value.
     */
    protected final T getLastTrustValue( String uidTarget )
    {
        return trustCache.get( uidTarget );
    }

    /**
     * This method requests the update of the trust relationship between the current entity and a trustee.
     * @param uidTarget     the unique identifier of the trustee.
     * @param reconfigure   it specifies whether, if it is necessary, a reconfiguration should be performed.
     * @throws TrustException
     */
    protected final void requestTrustUpdate( final String uidTarget, boolean reconfigure ) throws TrustException
    {
        if( role.equals("trustee") )
        {
            throw new TrustException( "Trustees cannot request trust relationships updates" );
        }

        String payload = trustContext + "." + uidTarget  + "." + uid + "." + String.valueOf( reconfigure );
        requestTrustUpdate.call(payload,
                                new Callback<String>() {
            @Override
            public void onSuccess( String result ) {
                //It seems difficult to retrieve an answer of a generic type; therefore, we use a String to
                //retrieve the result. This implies that, if we want users to define the type of the trust value,
                //we need that generic types implement the method toString(); and we need to provide
                //users with the means to convert from String to the type they want; we do this with a
                //protected method that users must override.
                onTrustRelationshipUpdated( uidTarget, result );
            }

            @Override
            public void onError(Throwable exception) {
                try {
                    launchException( "Error while returning trust update: "+ exception.getMessage() );
                } catch (TrustException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * This method requests the update of the trust relationship between the current entity and a trustee. It is assumed
     * that a reconfiguration, in case it is required, is desired.
     * @param uidTarget the unique identifier of the trustee.
     * @throws TrustException
     */
    protected final void requestTrustUpdate( final String uidTarget ) throws TrustException
    {
        requestTrustUpdate(uidTarget, true);
    }

    /**
     * This method is called by the trust framework when a new trust value is available for a trust relationship between
     * this entity and a trustee entity. Clients should override this method to retrieve the trust information and act accordingly.
     * @param uidTarget the unique identifier of the trustee.
     * @param newVal    newVal(0) is the new trust value; if the trust model computes a trust threshold, newVal(1) is the threshold.
     *                  Otherwise, newVal(1) is null
     */
    protected void trustRelationshipUpdated( final String uidTarget, List<T> newVal ) { }


    /**
     * This method is called by the trust framework before handling the trust value to the client. Clients should override it in
     * order to specify how the translation from String to the generic type should be performed.
     * @param values    a list containing the trust value (and optionally the threshold value) encoded in a String.
     * @return          a list containing the trust value (and optionally the threshold value) encoded in the generic type.
     */
    protected List<T> convertBackToValuesTypes( List<String> values )
    {
        return null;
    }
    /**
     * It returns the unique identifier of this entity.
     * @return  the unique identifier.
     */
    protected final String getUID()
    {
        return uid;
    }

    /**
     * It returns the group to which this entity belongs, or null if it does belong to no group.
     * @return  the name of the group.
     */
    protected final String getBelongingGroup()
    {
        return group;
    }

    /**
     * It launches a trust exception.
     * @param s message of the exception.
     * @throws TrustException
     */
    private void launchException (String s) throws TrustException
    {
        throw new TrustException( s );
    }

    /**
     * This method is called by the trust framework when an update of a trust relationship has been performed.
     * @param uidTarget the unique identifier of the trustee.
     * @param newVal    the new value of the trust relationship, which may include the value of a threshold.
     */
    private void onTrustRelationshipUpdated( final String uidTarget, String newVal )
    {
        JsonArray jsonArray = JsonArray.readFrom( newVal );

        List<String> values = new ArrayList();
        values.add( jsonArray.get( 0 ).asString() );
        if ( jsonArray.size() > 1 )
        {
            values.add(jsonArray.get(1).asString());
        }
        List<T> actualValues = convertBackToValuesTypes( values );

        trustCache.put(uidTarget, actualValues.get(0));
        trustRelationshipUpdated( uidTarget, actualValues );
    }

    /**
     * Utility method to read the file where the subjective factors are defined
     */
    private void storeSubjectiveFactors()
    {
        Path path = Paths.get( subjectiveFactorsFilePath );
        List<String> subjectiveFactors = null;
        try {
            subjectiveFactors = Files.readAllLines(path, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for( String sf : subjectiveFactors )
        {
            StringTokenizer st = new StringTokenizer( sf );
            String factorName = st.nextToken();
            String factorValue = st.nextToken();
            String target = uid; //assume at first that the target is the trustor itself
            if ( st.hasMoreTokens() )
            {
                target = st.nextToken();
            }
            FactorInfo<F> fi = new FactorInfo( trustContext, uid, factorName, factorValue, factorValue, target, Calendar.getInstance().getTime().getTime() );
            addFactor.send( fi );
        }
    }

    //------------------------------------
    //Model Listener methods
    //------------------------------------
    @Override
    public boolean preUpdate(UpdateContext context) {
        return true;
    }

    @Override
    public boolean initUpdate(UpdateContext context) {
        return true;
    }

    @Override
    public boolean afterLocalUpdate(UpdateContext context) {
        return true;
    }

    //Everytime there's a model update, we want to recompute trustees
    //We cannot do this in "start()" because there is no information abou the model yet
    @Override
    public void modelUpdated() {

        //After a model update, I might have been deleted, so I have to check if I'm still in the model
        boolean inTheModel = GetHelper.isInTheModel(model.getCurrentModel().getModel(), new StringTokenizer(uid, "@").nextToken());
        if( inTheModel && ( "trustor".equals( role ) || "both".equals( role ))) {

            //initTrustRelationships.send(trustContext + "." + uid + "." + bootstrappingTrustValue);
            //Get all the trustees to this trustor
            //trustees store: [Node 0, (CompInstance1, CompInstance2, etc)]
            //                [Node 1, (CompInstance34, CompInstance50, etc)]
            //We need this because the same instance name can be used in different nodes, but for the trust model
            //we need a unique identifier for trustees. In this case, "nodeName + instanceName"
            String trustorInstanceName = new StringTokenizer( uid, "@" ).nextToken();
            HashMap<String, List<String>> trusteesAndNodes =
                    GetHelper.getTrusteesInstanceName( model.getCurrentModel().getModel(), trustContext, trustorInstanceName );
            trustees = new ArrayList<String>();

            //For every node in the model...
            for (String nodeName : trusteesAndNodes.keySet()) {
                //...get the list of trustees running on that node
                for (String compName : trusteesAndNodes.get(nodeName)) {
                    //The uid of a component is of the form: compInstance@nodeWhereRunning
                    trustees.add(compName + "@" + nodeName);
                }
            }

            for (String trustee : trustees)
            {
                long ts = Calendar.getInstance().getTime().getTime();
                TrustRelationInfo tri = new TrustRelationInfo( trustContext, uid, trustee, bootstrappingTrustValue, ts );
                initTrustRelationships.send( tri );
            }
        }
    }

    @Override
    public void preRollback(UpdateContext context) {

    }

    @Override
    public void postRollback(UpdateContext context) {

    }

    /*
    protected final void addSubjectiveFactor( String name, F value )
    {
        if( "trustor".equals( role ) || "both".equals( role ))
        {
            FactorInfo<F> fi = new FactorInfo(trustContext, uid, name, value, uid, Calendar.getInstance().getTime().getTime());
            addFactor.send(fi);
        }
    }

    protected final void addSubjectiveFactor( String name, F value, String uidTarget )
    {
        if( "trustor".equals( role ) || "both".equals( role ))
        {
            FactorInfo<F> fi = new FactorInfo(trustContext, uid, name, value, uidTarget, Calendar.getInstance().getTime().getTime());
            addFactor.send(fi);
        }
    }
    */
}



