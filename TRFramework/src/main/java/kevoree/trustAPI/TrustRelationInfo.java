package kevoree.trustAPI;

/**
 * Created by franciscomoyanolara on 02/12/14.
 */
final class TrustRelationInfo {

    private String context;
    private String idTrustor;
    private String idTrustee;
    private String value;
    private long timeStamp;

    TrustRelationInfo( String ctx, String trustor, String trustee, String val, long ts )
    {
        context = ctx;
        idTrustor = trustor;
        idTrustee = trustee;
        value = val;
        timeStamp = ts;
    }

    String getContext()
    {
        return context;
    }

    String getIdTrustor()
    {
        return idTrustor;
    }

    String getIdTrustee()
    {
        return idTrustee;
    }

    String getValue()
    {
        return value;
    }

    long getTimeStamp()
    {
        return timeStamp;
    }

}
