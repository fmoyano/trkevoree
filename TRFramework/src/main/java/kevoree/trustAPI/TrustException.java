package kevoree.trustAPI;

/**
 * This class encapsulates an exception that the trust framework may throw.
 */
public final class TrustException extends Exception {

    /**
     * Default ctor.
     */
    TrustException()
    {
        super();
    }

    /**
     * Ctor taking a message.
     * @param message   the message to be shown.
     */
    TrustException( String message )
    {
        super( message );
    }
}
