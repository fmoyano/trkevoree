package kevoree.trustAPI;

import com.eclipsesource.json.JsonArray;
import kevoree.trCommon.kevReflection.GetHelper;
import kevoree.trCommon.kevReflection.ScriptEngine;
import kevoree.trCommon.IComputationEngine;
import kevoree.trCommon.Tuple;
import org.kevoree.annotation.*;
import org.kevoree.api.ModelService;
import trustmetamodel.*;
import trustmetamodel.factory.DefaultTrustMetamodelFactory;
import trustmetamodel.factory.TrustMetamodelFactory;

import java.util.*;

/**
 * This class represents a trust model. Both generic types must implement the method toString(); otherwise, behaviour is undefined.
 * @param <T>   type of the value that the model computes. Type of the threshold if the model computes it.
 * @param <F>   type of the factors that the model accepts.
 */
@ComponentType
public class TrustModel<T,F> implements IComputationEngine {

    /**
     * Trust metamodel objects.
     */
    private TrustMetamodelFactory factory = null;
    private TrustRoot trustModel = null;

    /**
     * Kevoree context.
     */
    @KevoreeInject
    private org.kevoree.api.Context context;

    /**
     * Access to the Kevoree reflection layer.
     */
    @KevoreeInject
    private ModelService model;

    /**
     * Engine for trust rules processing.
     */
    private TrustRulesEngine tre;

    /**
     * Engine for the script execution.
     */
    private ScriptEngine se;

    /**
     * This method is called by the Kevoree runtime at start-up. It should be explicitly invoked by
     * sub-classes in the first line of their start methods.
     * @param fileName  the name of the file with the reconfiguration policies.
     * @throws TrustException
     */
    @Start
    public void start( String fileName ) throws TrustException
    {
        factory = new DefaultTrustMetamodelFactory();
        trustModel = factory.createTrustRoot();
        if(  trustModel == null )
        {
            throw new TrustException( "Trust Model initialization error" );
        }
        se = new ScriptEngine( model );
        tre = new TrustRulesEngine( fileName, se );
    }

    /**
     * This method is called by the Kevoree runtime at start-up. It should be explicitly invoked by
     * sub-classes in the first line of their start methods. It assumes that the name of the policy file
     * is TrustRules.policy.
     * @throws TrustException
     */
    protected void start() throws TrustException
    {
        start( "TrustRules.policy" );
    }

    /**
     * This method receives a factor.
     * @param fi    the factor information.
     */
    @Input
    protected final void receiveFactor( Object fi ) {

        String context = null;
        String target = null;
        String name = null;
        String source = null;
        String increment = null;
        String initialValue = null;
        long timeStamp = 0;

        if (fi instanceof FactorInfo) {
            context = ((FactorInfo) fi).getContext();
            source = ((FactorInfo) fi).getSource();
            name = ((FactorInfo) fi).getName();
            increment = ((FactorInfo) fi).getValue().toString();
            initialValue = ((FactorInfo) fi).getInitialValue().toString();
            target = ((FactorInfo) fi).getTarget();
            timeStamp = ((FactorInfo) fi).getTimeStamp();
        }

        //The id of a factor is given by the name of the factor and the target of the factor
        Factor factor = trustModel.findFactorsByID( source + name + target );

        //If it doesn't exist, we create it and set its properties
        if ( factor == null )
        {
            factor = factory.createFactor();
            factor.setIdFactor( source + name + target );
            factor.setContext( context );
            factor.setIdSender( source );
            factor.setIdTarget( target );
            factor.setName( name );
            FactorValue fVal = factory.createFactorValue();
            fVal.setValue( initialValue );
            fVal.setTimeStamp( String.valueOf( timeStamp ));
            factor.setValue( fVal );
        }
        //If it exists, but we don't want to change (increment) it, we simply change the value
        else if ( factor != null && increment == null )
        {
            String newVal = initialValue;
            FactorValue fVal = factory.createFactorValue();
            fVal.setValue( newVal );
            fVal.setTimeStamp( String.valueOf( timeStamp ));
            factor.setValue( fVal );
        }
        //If it exists and we want to change the factor, we take the original value and increment it
        else
        {
            //If it exists, we have to take the value and increment it
            String newVal = incrementFactor( factor.getValue().getValue(), increment );
            FactorValue fVal = factory.createFactorValue();
            fVal.setValue( newVal );
            fVal.setTimeStamp( String.valueOf( timeStamp ));
            factor.setValue( fVal );
        }
        trustModel.addFactors( factor );
    }

    /**
     * This method initializes trust relationships.
     * @param request   trust relationship information.
     */
    @Input
    protected final void initializeTrustRelationships( Object request )
    {
        String context = null;
        String idTrustor = null;
        String idTrustee = null;
        String bootstrappingTrustValue = null;
        long ts = 0;
        if ( request instanceof  TrustRelationInfo )
        {
            context = ((TrustRelationInfo) request).getContext();
            idTrustor = ((TrustRelationInfo) request).getIdTrustor();
            idTrustee = ((TrustRelationInfo) request).getIdTrustee();
            bootstrappingTrustValue = ((TrustRelationInfo) request).getValue();
            ts = ((TrustRelationInfo) request).getTimeStamp();
        }

        addTrustRelationship( context, idTrustor, idTrustee, bootstrappingTrustValue, ts );
    }

    /**
     * This method computes a new trust (and optionally threshold) value according to the information in the request.
     * @param request   request information.
     * @return  the updated trust value.
     */
    @Input
    protected String requestTrustUpdate( Object request )
    {
        StringTokenizer st = new StringTokenizer( request.toString(), "." );
        String context = st.nextToken();
        String uidTrustee = st.nextToken();
        String uidTrustor = st.nextToken();
        boolean reconfigure = Boolean.parseBoolean( st.nextToken() );

        T threshold = computeThreshold( context, uidTrustee, uidTrustor );

        //we put compute after computeThreshold because in compute we might create new factors but the threshold should
        //use the original factors; no the modified ones
        T trustValue = compute( context, uidTrustee, uidTrustor );

        if ( reconfigure )
        {
            tre.executeRules(model, uidTrustor, uidTrustee, trustValue.toString(), threshold.toString());
        }

        JsonArray jsonArray = new JsonArray().add( trustValue.toString() ).
                                              add( threshold.toString() );
        String returnValue = jsonArray.toString();

        return returnValue;
    }

    /**
     * This method computes a new trust value. It must be overriden by client classes in order to customize the way
     * this computation should be performed.
     * @param context   the context of the trust relationship.
     * @param idTrustee the unique identifier of the trustee.
     * @param idTrustor the unique identifier of the trustor.
     * @return  the new trust value for the trust relationship between the trustor and the trustee.
     */
    @Override
    public T compute(String context, String idTrustee, String idTrustor) {
        return null;
    }

    /**
     * This method computes a new threshold value. It must be overriden by client classes in order to customize the way
     * this computation should be performed. As opposed to compute() method, which is compulsory, the overriding of this method is optional.
     * @param context   the context of the trust relationship.
     * @param idTrustee the unique identifier of the trustee.
     * @param idTrustor the unique identifier of the trustor.
     * @return  the new threshold value associated to the trust relationship between the trustor and the trustee.
     */
    protected T computeThreshold( String context, String idTrustee, String idTrustor )  { return null; }

    /**
     * This method increments a factor. This class must be overriden by client classes to implement appropriately this increment.
     * @param currentValue  the current value of the factor.
     * @param increment     the desired increment over the current factor value.
     * @return  the result of the increment, encoded as a String.
     */
    protected String incrementFactor( String currentValue, String increment )
    {
        return null;
    }

    /**
     * This method adds information about a factor to the model.
     * @param context   the trust context.
     * @param name      the name of the factor.
     * @param value     the value of the factor.
     * @param idTrustor the unique identifier of the trustor that creates the factor.
     * @param idTrustee the unique identifier of the trustee to which the factor refers.
     */
    protected final void addFactor( String context, String name, F value, String idTrustor, String idTrustee )
    {
        //FactorInfo<F> fi = new FactorInfo( context, idTrustor, name, value, idTrustee, Calendar.getInstance().getTime().getTime() );
        //receiveFactor( fi );
        long timeStamp = Calendar.getInstance().getTime().getTime();

        //The id of a factor is given by the name of the factor and the target of the factor
        Factor factor = trustModel.findFactorsByID( name + idTrustee );
        //If it doesn't exist, we create it and set its properties
        if ( factor == null )
        {
            factor = factory.createFactor();
            factor.setIdFactor( name + idTrustee );
            factor.setContext(context);
            factor.setIdSender(idTrustor);
            factor.setIdTarget(idTrustee);
            factor.setName(name);
        }
        //Either if it exists or not, we have to set its value
        FactorValue fVal = factory.createFactorValue();
        fVal.setValue(value.toString());
        fVal.setTimeStamp(String.valueOf(timeStamp));
        factor.setValue(fVal);

        trustModel.addFactors( factor );
    }

    //It gets the value of the subjective factor of a trustor about a trustee
    //e.g. belief of X in competence in Y ( context, "competence", "X", "Y")

    /**
     * This method retrieves the value of the subjective factor of a trustor about a trustee.
     * @param context   the trust context.
     * @param name      the name of the factor.
     * @param trustor   the unique idenfier of the trustor entity.
     * @param trustee   the unique identifier of the trustee entity.
     * @return          the value of the factor, encoded in a String.
     */
    protected final String getFactorValue( String context, String name, String trustor, String trustee )
    {
        System.out.println( trustor + " trying to retrieve factor " + name + " about " + trustee );
        for( Factor f : trustModel.getFactors() )
        {
            /*System.out.println("Factor info:" + f.getName() + " " + f.getContext() + " " +
                                f.getIdSender() + " " + f.getIdTarget() + " " + f.getValue().getValue() );          */
            if (context.equals(f.getContext()) && name.equals(f.getName()) &&
                    trustee.equals(f.getIdTarget()) && trustor.equals(f.getIdSender()))
            {
                return f.getValue().getValue();
            }
        }
        return null;
    }

    /**
     * This method retrieves the value of a subjective factor that refers to a given target.
     * @param context   the trust context.
     * @param name      the name of the factor.
     * @param uidTarget the unique identifier of the entity to which the factor refers.
     * @return  the value of the factor encoded in a String.
     */
    protected final String getFactorValue( String context, String name, String uidTarget )
    {
        for( Factor f : trustModel.getFactors() )
        {
            if (context.equals(f.getContext()) && name.equals(f.getName()) &&
                    uidTarget.equals(f.getIdTarget()))
            {
                return f.getValue().getValue();
            }
        }
        return null;
    }

    /**
     * This method retrieves information about a subjective factor that refers to a given entity.
     * @param context   the trust context.
     * @param name      the name of the factor.
     * @param uidTarget the unique identifier of the entity to which the factor refers.
     * @return  a tuple consisting of three elements: the value of the factor, the entity that issued the factor, and the time
     *          when the factor was created.
     */
    protected final Tuple<String,String,String> getFactorInfo( String context, String name, String uidTarget )
    {
        for( Factor f : trustModel.getFactors() )
        {
            if (context.equals(f.getContext()) && name.equals(f.getName()) &&
                    uidTarget.equals(f.getIdTarget()))
            {
                Tuple<String,String,String> t = new Tuple( f.getValue().getValue(),
                                                            f.getIdSender(),
                                                            f.getValue().getTimeStamp()
                                                        );
                return t;
            }
        }
        return null;
    }

    /**
     * This method retrieves the value of a parameter of an entity.
     * @param idComponent   the unique identifier of the entity.
     * @param param         the name of the parameter.
     * @return  the value of the parameter.
     */
    protected final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@" ).nextToken();
        return GetHelper.getParamFromInstance( model.getCurrentModel().getModel(), instance, param );
    }

    /**
     * This method adds a trust relationship to the trust metamodel.
     * @param context       the trust context.
     * @param idTrustor     the unique identifier of the trustor entity.
     * @param idTrustee     the unique identifier of the trustee entity.
     * @param initialValue  the initial value of the trust relationship.
     * @param timeStamp     a time stamp for the creatio.
     */
    private void addTrustRelationship( String context, String idTrustor,
                                       String idTrustee, String initialValue,
                                       long timeStamp )
    {

        System.out.println( "Adding relationship: " + context + " "
                + idTrustor + " " + idTrustee + " " + initialValue );

        Trustee trustee = trustModel.findTrusteesByID( idTrustee );
        if ( trustee == null )
        {
            trustee = factory.createTrustee();
            trustee.setIdTrustee( idTrustee );
            trustModel.addTrustees( trustee );
        }

        Trustor trustor = trustModel.findTrustorsByID( idTrustor );
        if ( trustor == null )
        {
            trustor = factory.createTrustor();
            trustor.setIdTrustor(idTrustor);
            trustModel.addTrustors( trustor );
        }
        if ( trustor.findTrustorTrusteesByID( idTrustee ) == null )
        {
            trustor.addTrustorTrustees( trustee );
        }

        TrustValue trustValue = factory.createTrustValue();
        trustValue.setValue( initialValue );
        trustValue.setTimeStamp( String.valueOf(timeStamp) );

        TrustRelationship trustRelationship = trustModel.findTRelationshipsByID(context + idTrustor + idTrustee);
        if ( trustRelationship == null )
        {
            trustRelationship = factory.createTrustRelationship();
            trustRelationship.setContext(context);
            trustRelationship.setTrustor(trustor);
            trustRelationship.setTrustee(trustee);
            trustRelationship.setIdTRelationship( context + idTrustor + idTrustee );
            trustModel.addTRelationships( trustRelationship );

        }
        trustRelationship.addTrustValue( trustValue );
    }

    /*@Input
    private void receiveTrustRelationship( Object tr )
    {
        String context = null;
        String idTrustor = null;
        String idTrustee = null;
        String value = null;
        String timeStamp = null;

        if ( tr instanceof  TrustRelationInfo )
        {
            context = ((TrustRelationInfo) tr).getContext();
            idTrustor = ((TrustRelationInfo) tr).getIdTrustor();
            idTrustee = ((TrustRelationInfo) tr).getIdTrustee();
            value = ((TrustRelationInfo) tr).getValue();
            timeStamp = ((TrustRelationInfo) tr).getTimeStamp();
        }

        Trustee trustee = trustModel.findTrusteesByID( idTrustee );
        if ( trustee == null )
        {
            trustee = factory.createTrustee();
            trustee.setIdTrustee( idTrustee );
            trustModel.addTrustees( trustee );
        }

        Trustor trustor = trustModel.findTrustorsByID( idTrustor );
        if ( trustor == null )
        {
            trustor = factory.createTrustor();
            trustor.setIdTrustor(idTrustor);
            trustModel.addTrustors( trustor );
        }
        if ( trustor.findTrustorTrusteesByID( idTrustee ) == null )
        {
            trustor.addTrustorTrustees( trustee );
        }

        TrustValue trustValue = factory.createTrustValue();
        trustValue.setValue( value );
        trustValue.setTimeStamp( timeStamp );

        TrustRelationship trustRelationship = trustModel.findTRelationshipsByID(context + idTrustor + idTrustee);
        if ( trustRelationship == null )
        {
            trustRelationship = factory.createTrustRelationship();
            trustRelationship.setContext(context);
            trustRelationship.setTrustor(trustor);
            trustRelationship.setTrustee(trustee);
            trustRelationship.setIdTRelationship( context + idTrustor + idTrustee );
            trustModel.addTRelationships( trustRelationship );

        }
        trustRelationship.addTrustValue( trustValue );
    }  */

      /*protected final Tuple<String,String,String> getFactorInfo( String context, String name, String trustor, String trustee )
    {
        for( Factor f : trustModel.getFactors() )
        {
            if (context.equals(f.getContext()) && name.equals(f.getName()) &&
                    trustee.equals(f.getIdTarget()) && trustor.equals(f.getIdSender()))
            {
                Tuple<String,String,String> t = new Tuple( f.getValue().getValue(),
                        f.getIdSender(),
                        f.getValue().getTimeStamp()
                );
                return t;
            }
        }
        return null;
    }  */

    /*@Input
    protected final void initializeTrustRelationships( Object request )
    {
        StringTokenizer st = new StringTokenizer( request.toString(), "." );
        String context = st.nextToken();
        String idTrustor = st.nextToken();
        String bootstrappingTrustValue = st.nextToken();

        //Get all the trustees to this trustor
        //trustees store: [Node 0, (CompInstance1, CompInstance2, etc)]
        //                [Node 1, (CompInstance34, CompInstance50, etc)]
        //We need this because the same instance name can be used in different nodes, but for the trust model
        //we need a unique identifier for trustees. In this case, "nodeName + instanceName"
        String trustorInstanceName = new StringTokenizer( idTrustor, "@" ).nextToken();
        HashMap<String, List<String>> trustees = GetHelper.getTrusteesInstanceName( model.getCurrentModel().getModel(), context, trustorInstanceName );
        List<String> idTrustee = new ArrayList<String>();

        //For every node in the model...
        for (String nodeName: trustees.keySet()) {
            //...get the list of trustees running on that node
            for ( String compName : trustees.get( nodeName ))
            {
                //The uid of a component is of the form: compInstance@nodeWhereRunning
                idTrustee.add( compName + "@" + nodeName );
            }
        }

        //Now, we have all the information regarding the new trust relationship
        //We know: trustor, trustee, context and metric
        //We have to create all the necessary entities in the trust metamodel
        for (String t : idTrustee) {
            long ts = Calendar.getInstance().getTime().getTime();
            addTrustRelationship( context, idTrustor, t, bootstrappingTrustValue, ts );
        }
    }   */
}



