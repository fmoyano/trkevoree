package kevoree.trustAPI;

import kevoree.trCommon.kevReflection.GetHelper;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.ModelService;
import org.kevoree.api.Port;

import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * A factor produces is an entity that is in charge of creating objective factors about other entities.
 * @param <F>   type of the factor created by the entity.
 */
@ComponentType
public class FactorProducer<F> implements Runnable {

    /**
     * Operating mode of the factor producer.
     * ASSIGNED: the factor producer assigns at bootstrap time the value.
     * MONITORED: the factor producer uses low-level services of the platform (e.g. packet loses) to calculate the value.
     */
    protected enum How { ASSIGNED, MONITORED }

    /**
     * Unique identifier of the factor producer.
     */
    @Param
    private String uid;

    /**
     * Unique identifier of the target to which the factor applies.
     */
    @Param
    private String uidTarget;

    /**
     * Trust context.
     */
    @Param( defaultValue = "MyContext")
    private String trustContext;

    /**
     * Time interval between monitoring in milliseconds.
     */
    @Param( defaultValue = "5000" )
    private String monitoredTime;

    /**
     * Context service provided by Kevoree.
     */
    @KevoreeInject
    private Context context;

    /**
     * For Kevoree model access.
     */
    @KevoreeInject
    private ModelService model;

    /**
     * Port to send factor to the trust model.
     */
    @Output
    private Port sendFactor;


    private String factorName;
    private How how;
    private Thread myThread;
    boolean alive;

    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     * @param fn    factor name.
     * @param h     mode of assignment: assigned or monitored.
     */
    @Start
    public void start( String fn, How h )
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();
        how = h;
        factorName = fn;

        if( h == How.ASSIGNED )
        {
            addFactorValue( doEvaluation(uidTarget) );
        }
        else
        {
            myThread = new Thread( this );
            alive = true;
            myThread.start();
        }
    }

    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     */
    @Stop
    public void stop()
    {
        alive = false;
        myThread.interrupt();
    }

    /**
     * This method is automatically called by the Kevoree framework. It should be called in the first line of any class
     * overriding this method.
     */
    @Update
    public void update()
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();

        if( how == How.MONITORED )
        {
            alive = true;
            myThread = new Thread(this);
            myThread.start();
        }
    }

    /**
     * Send the factor value to the model.
     * @param value factor value.
     */
    private void addFactorValue( F value )
    {
        //We put to increment to null because we don't want to change an existing factor value, but to replace it
        FactorInfo<F> fi = new FactorInfo( trustContext, uid, factorName, null, value, uidTarget,
                Calendar.getInstance().getTime().getTime() );
        sendFactor.send( fi );
    }

    /**
     * It retrieves the value of a parameter of a component instance.
     * @param idComponent   unique identifier of the component instance.
     * @param param         name of the parameter.
     * @return              value of the parameter.
     */
    protected final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@" ).nextToken();
        System.out.println("Calling getParam with param " + param + " and instance " + instance);
        return GetHelper.getParamFromInstance(model.getCurrentModel().getModel(), instance, param);
    }

    /**
     * This method is executed if the mode of operation is Monitored.
     */
    @Override
    public final void run()
    {
        while( alive )
        {
            F res = doEvaluation( uidTarget );
            addFactorValue( res );

            try {
                Thread.sleep( Long.valueOf( monitoredTime ));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Evaluates the factor for a component instance.
     * @param uidTarget unique identifier of the component under evaluation.
     * @return  result of the evaluation.
     */
    protected F doEvaluation( String uidTarget )
    {
        return null;
    }
}
