package kevoree.appDistRep;

import kevoree.reputationAPI.ReputationEngine;
import kevoree.reputationAPI.ReputationStatementInfo;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by franciscomoyanolara on 22/01/15.
 */
public class PeerTrustModel extends ReputationEngine<Float>
{

    private float calculateTotalReputation( String idTarget )
    {
        List<ReputationStatementInfo> allStatementsAboutTarget = getClaimsAboutTarget( "CleanWords", idTarget );
        Set<String> consideredEntities = new HashSet();
        float totalRep = 0.0f;
        for ( ReputationStatementInfo rs : allStatementsAboutTarget )
        {
            String sourceEntity = rs.getSource();
            String sourceReputationString = getLastReputation( sourceEntity );
            float sourceReputation = 0.0f;
            if( sourceReputationString != null && !consideredEntities.contains( sourceEntity ))
            {
                sourceReputation = Float.valueOf( sourceReputationString );
            }
            consideredEntities.add( sourceEntity );
            totalRep += sourceReputation;
        }
        String targetReputationString = getLastReputation( idTarget );
        float targetReputation = 0.0f;
        if ( targetReputationString != null )
        {
            targetReputation = Float.valueOf( targetReputationString );
        }
        totalRep += targetReputation;
        return totalRep;
    }

    @Override
    public Float compute(String context, String idTarget, String idSource)
    {
        //System.out.println("PeerTrust computing reputation");
        List<ReputationStatementInfo> allStatementsAboutTarget = getClaimsAboutTarget( "CleanWords", idTarget );

        float totalReputation = calculateTotalReputation( idTarget );

        double currentTime = (double) Calendar.getInstance().getTime().getTime();
        float targetReputation = 0.0f;
        //For each claim (transaction in PeerTrust), we have to check the source, and then, we have to calculate the reputation of the agent
        for ( ReputationStatementInfo rs : allStatementsAboutTarget )
        {
            String source = rs.getSource();
            String sourceReputationString = getLastReputation(source);
            float sourceReputation = 0.0f;
            if ( sourceReputationString != null )
            {
                sourceReputation = Float.valueOf( sourceReputationString );
            }
            else
            {
                //System.out.println( "Source of the claim is " + rs.getSource() + "but we don't know its reputation yet" );
            }
            float credibility = 0.5f;
            if( totalReputation != 0.0f )
            {
                credibility = sourceReputation / totalReputation;
            }

            //System.out.println("trust factor context is: " + Double.valueOf( rs.getTimeStamp() ) / currentTime );
            targetReputation += Float.valueOf( rs.getClaim().getValue() ) *  //claim value
                                                credibility *       //credibility of the claim
                                                Double.valueOf( rs.getTimeStamp() ) / currentTime;  //context(trust factor context: time stamp in this case)

            //System.out.println( "Target reputation is " + targetReputation );
        }
        return targetReputation;
    }
}
