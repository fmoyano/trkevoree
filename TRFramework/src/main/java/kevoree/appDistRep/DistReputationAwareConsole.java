package kevoree.appDistRep;

import kevoree.appUtilCommon.ConsolePanel;
import kevoree.appUtilCommon.TextTypedLocallyInterface;
import kevoree.reputationAPI.DistKevReputableEntity;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.Port;
import org.kevoree.library.java.core.console.ConsoleFrame;
import org.kevoree.library.java.core.console.TabbedConsoleFrame;
import org.kevoree.log.Log;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by franciscomoyanolara on 21/11/14.
 */
@ComponentType
public class DistReputationAwareConsole extends DistKevReputableEntity<Float> implements TextTypedLocallyInterface
{
    private ConsolePanel<DistReputationAwareConsole> thisConsole;
    private ConsoleFrame standaloneFrame;

    @Param(defaultValue = "true")
    protected Boolean showInTab = true;

    @Output
    protected Port textEntered;

    @KevoreeInject
    protected Context cmpContext;

    /**
     * Uniquely identifies the console in tabs panels. Also used as title for the Standalone frame.
     */
    private String consoleKey;

    private Map<String,String> lastMessageReceived;


    @Start
    public void startConsole()
    {
        super.start( new PeerTrustModel() );

        //Uniquely identifies the console
        consoleKey = getUID();
        thisConsole = new ConsolePanel( this );
        thisConsole.appendSystem("/***** CONSOLE READY ******/ ");

        if(showInTab) {
            TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    standaloneFrame = new ConsoleFrame(consoleKey);
                    standaloneFrame.init(thisConsole);
                }
            });
        }

        lastMessageReceived = new HashMap();
    }

    @Stop
    public void stop()
    {
        if(showInTab) {
            TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
            Log.debug("Stopped from TAB");
        } else {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
            }
        }
        standaloneFrame = null;
        thisConsole = null;
        consoleKey = null;
    }


    @Update
    public void update()
    {
        super.update();
        if(showInTab) {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
                standaloneFrame = null;
                consoleKey = getUID();
                TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
            }
        } else {
            if(standaloneFrame == null) {
                TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
                consoleKey = getUID();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        standaloneFrame = new ConsoleFrame(consoleKey);
                        standaloneFrame.init(thisConsole);
                    }
                });
            }
        }
    }

    public void appendSystem(String text)
    {
        thisConsole.appendSystem(text);
    }


    public void textTypedLocally( String text )
    {
        long currentTime = System.nanoTime();
        System.out.println( "Current time: " + currentTime );
        textEntered.send( getUID() + "."
                + getBelongingGroup()  + "."
                + currentTime + "."
                + text );
    }

    @Input
    public void showText(Object text)
    {
        if( text != null ) {
            String msg = text.toString();
            StringTokenizer st = new StringTokenizer( msg, "." );
            String idTarget = st.nextToken();
            String group = st.nextToken();
            String message = st.nextToken() + "." + st.nextToken();
            lastMessageReceived.put( idTarget, message );

            if( "shit\n".equals( message ) || "fuck\n".equals( message ) )
            {
                System.out.println("Bad claim made by " + getUID());
                makeClaim("CleanWords", "0", idTarget);
            }
            else
            {
                System.out.println("Good claim made by " + getUID());
                makeClaim( "CleanWords", "1", idTarget );
            }
            requestReputation(idTarget);

        }
    }

    @Override
    protected void reputationReceived(String idTarget, Float newVal) {

        StringTokenizer st = new StringTokenizer( lastMessageReceived.get( idTarget ), ".");
        long timeSent = Long.valueOf(st.nextToken());
        long timeReceived = System.nanoTime();
        long timeElapsed = timeReceived - timeSent;

        System.out.println( "Time sent: " + timeSent );
        System.out.println( "Time received: " + timeReceived );
        System.out.println( "Time elapsed " + getUID() + ": " + timeElapsed );

        thisConsole.appendIncomming( st.nextToken() );

    }
}

