package kevoree.appDistRep;

import kevoree.reputationAPI.ReputationEngine;
import kevoree.reputationAPI.ReputationStatementInfo;
import kevoree.trCommon.Tuple;
import reputationmetamodel.ReputationStatement;

import java.util.Calendar;
import java.util.List;

/**
 * Created by franciscomoyanolara on 25/11/14.
 */
public class RegretReputationModel extends ReputationEngine<Float> {

    @Override
    public Float compute(String context, String idTarget, String idSource) {

        //1) Calculate subjective reputation
        System.out.println("Calculating subjective reputation");
        List<ReputationStatementInfo> claims = getClaims( "CleanWords", idSource, idTarget );
        float subjectiveReputation = 0.0f;
        float totalClaims = claims.size();
        double currentTime = (double) Calendar.getInstance().getTime().getTime();
        for( ReputationStatementInfo rs : claims )
        {
            float claimVal = Float.valueOf( rs.getClaim().getValue() );
            double claimTimeStamp = (double) rs.getTimeStamp();
            subjectiveReputation += (claimVal / totalClaims) * (claimTimeStamp / currentTime);
        }
        System.out.println("Subjective reputation = " + subjectiveReputation);

        //2) Now, retrieve all the claims that idSource made about any entity in the same group as idTarget
        String groupTarget = getParam( idTarget, "group" );
        List<ReputationStatementInfo> targetGroupClaims = getClaimsFromSource("CleanWords", idSource);
        float targetGroupReputation = 0.0f;
        currentTime =  Calendar.getInstance().getTime().getTime();
        System.out.println( idSource + " is calculating target group reputation: " + targetGroupClaims.size() );
        for( ReputationStatementInfo rs: targetGroupClaims )
        {
            //We don't want to consider claims about the target itself
            if ( !idTarget.equals( rs.getTarget() ))
            {
                String g = getParam(rs.getTarget(), "group");
                if (groupTarget.equals(g)) {
                    System.out.println( "The group of " +rs.getTarget() + " is the same as the group of " + idTarget + ": " + g );
                    float claimVal = Float.valueOf(rs.getClaim().getValue());
                    double claimTimeStamp = (double) rs.getTimeStamp();
                    targetGroupReputation += (claimVal / totalClaims) * (claimTimeStamp / currentTime);
                }
            }
        }
        System.out.println( "Target group reputation = " + targetGroupReputation );

        //3) Now, retrieve all the claims that any entity in the same group as idSource made about idTarget
        String groupSource = getParam( idSource, "group" );
        List<ReputationStatementInfo> sourceGroupClaims = getClaimsAboutTarget( "CleanWords", idTarget );
        float sourceGroupReputation = 0.0f;
        currentTime =  Calendar.getInstance().getTime().getTime();
        System.out.println( idSource + " is calculating source group reputation: " + sourceGroupClaims.size() );
        for( ReputationStatementInfo rs : sourceGroupClaims )
        {
            //We don't want to consider claims from the source itself
            if ( !idSource.equals( rs.getSource() ))
            {
                String g = getParam(rs.getSource(), "group");
                if ( g != null && groupSource.equals(g) ) {
                    System.out.println( "The group of " + rs.getSource() + " is the same as " + idSource );
                    float claimVal = Float.valueOf( rs.getClaim().getValue() );
                    double claimTimeStamp = (double) rs.getTimeStamp();
                    sourceGroupReputation += (claimVal / totalClaims) * (claimTimeStamp / currentTime);
                }
            }
        }
        System.out.println( "Source group reputation = " + sourceGroupReputation );

        //System.out.println(" Final reputation: " + new Float( subjectiveReputation + targetGroupReputation + sourceGroupReputation ));
        return new Float( subjectiveReputation + targetGroupReputation + sourceGroupReputation );
    }
}
