package kevoree.trCommon.kevReflection;

import kevoree.trCommon.identity.IdentityProvider;
import org.kevoree.api.ModelService;
import org.kevoree.api.handler.UpdateCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by franciscomoyanolara on 12/11/14.
 *
 * This class provides an abstraction layer over the Kevoree script syntax. Clients of this class
 * call execute method to execute Kevoree scripts.
 */
public final class ScriptEngine {

    /**
     * Instance of the Kevscript engine.
     */
    private ModelService model;

    /**
     * The constructor takes an instance of the Kevscript engine.
     * @param m service for accessing the Kevoree model.
     */
    public ScriptEngine( ModelService m )
    {
         this.model = m;
    }

    /**
     * It executes an action in Kevscript.
     * @param action    the action to perform.
     * @param idTarget  the unique identifier of the component on which the action will be applied.
     * @param args      a list of args needed by the action.
     */
    public void execute( String action, String idTarget, List<String> args )
    {
        if( "substitute".equals( action ))
        {
            //If there is more than one argument, there are attributes
            if ( args.size() > 1 )
            {
                substituteComponent( idTarget, args.get(0), args.subList( 1, args.size() ));
            }
            else
            {
                substituteComponent( idTarget, args.get(0), null );
            }
        }
        else if( "remove".equals( action ))
        {
            removeComponent( idTarget );
        }
    }

    /**
     * It substitutes a component instance by a new component of a given type name.
     * The constraint is that the new component type must have at least the same
     * port names (both required and provided) than the old component instance.
     * @param idComponent   the unique identifier of the component instance.
     * @param newTypeName   the type name of the new component.
     * @param attributes    a list with the names of the attributes of the component instance.
     */
    private void substituteComponent( String idComponent, String newTypeName, List<String> attributes )
    {
        System.out.println("Substituting " + idComponent + " with one of type name " + newTypeName );
        /*Create a new id for the new component
        String cType = GetHelper.getComponentTypeFromInstance( model, instance );
        String newIdComponent = IdentityProvider.getIdentity( cType, st.nextToken() );
        System.out.println( "New component id: " + newIdComponent );    */

        StringTokenizer st = new StringTokenizer( idComponent, "@" );
        String instance = st.nextToken();
        String node = st.nextToken();

        //1st Obtain information (if necessary) about the current attributes of the instance
        Map<String,String> attr = new HashMap();
        if ( attributes == null )
        {
            System.out.println("New attributes should be exactly the same as the previous ones");
            attr = GetHelper.getAttributesInfo( model.getCurrentModel().getModel(), instance );
        }
        else
        {
            int i = 0;
            while( i < attributes.size() )
            {
                attr.put( attributes.get( i ), attributes.get( i + 1 ));
                i += 2;
            }
            for ( String s : attr.keySet() )
            {
                System.out.println( "Key: " + s + " Value: " + attr.get( s ));
            }

        }

        //2nd Obtain information about the bindings and channels of the component to remove
        HashMap<String, String> bindingInfo = GetHelper.getBindingsInfo( model.getCurrentModel().getModel(), instance );

        //3rd Remove component
        removeComponent(idComponent);

        //4th Create new instance name of type newTypeName
        String newIdComponent = IdentityProvider.getIdentity( newTypeName, node );
        String newInstance = new StringTokenizer( newIdComponent, "@").nextToken();

        //5th Write script that adds the component and link it to the bindings
        String script = "";
        script += "add " + node + "." + newInstance + ":" + newTypeName;
        script += "\n";
        for( String port : bindingInfo.keySet() )
        {
            script += "bind " + node + "." + newInstance + "." + port + " " + bindingInfo.get(port);
            script += "\n";
        }

        //6th Write script that adds the attributes to the new instance
        for( String attributeName : attr.keySet() )
        {
            script += "set " + node + "." + newInstance + "." + attributeName + " = " +
                                                            "\"" + attr.get( attributeName ) + "\"";
            script += "\n";
        }

        //7th Execute script
        model.submitScript(script, new UpdateCallback() {
            @Override
            public void run(Boolean applied) {

            }
        });
    }

    /**
     * It removes a component instance.
     * @param idComponent   uid of the component to be removed.
     */
    private void removeComponent( String idComponent )
    {
        StringTokenizer st = new StringTokenizer( idComponent, "@" );
        String instance = st.nextToken();
        String node = st.nextToken();

        String script = "remove " + node + "." + instance;
        model.submitScript(script, new UpdateCallback() {
            @Override
            public void run(Boolean applied) {

            }
        });
    }
}
