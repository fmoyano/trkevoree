package kevoree.trCommon.kevReflection;

import org.kevoree.*;
import java.util.*;


/**
 * Created by Francisco Moyano on 21/11/14.
 *
 * This static class offers a series of utility methods for querying the Kevoree
 * reflection layer. Its goal is to abstract developers from the inner details of
 * Kevoree models.
 */
public final class GetHelper {

    /**
     * This method returns a list with the names of all component instances of a component type
     * that are running on a node.
     * @param model         the root of the Kevoree model.
     * @param componentType the type of the component instance.
     * @param nodeName      the name of the node.
     * @return              a list of names of component instances.
     */
    public static List<String> getComponentInstanceName(ContainerRoot model, String componentType, String nodeName) {

        List<String> components = new ArrayList<String>();

        for (ContainerNode node : model.getNodes()) {
            if(node.getName().equals(nodeName)) {
                for(ComponentInstance component:node.getComponents()) {
                    if(component.getTypeDefinition().getName().equals(componentType)) {
                        components.add(component.getName());
                    }
                }
            }
        }

        return components;
    }

    /**
     * It returns a list with the names of all component instances running on every node in the model.
     * @param model         the root of the Kevoree model.
     * @param componentType the type of the component instance.
     * @return              a list of component instance names.
     */
    public static List<String> getComponentInstanceName(ContainerRoot model, String componentType) {

        List<String> components = new ArrayList<String>();

        for (ContainerNode node : model.getNodes()) {
            components.addAll(getComponentInstanceName(model, componentType, node.getName()));
        }
        return components;
    }


    /**
     * It returns the type name of a component instance.
     * @param model     the root of the Kevoree model.
     * @param idName    component instance name
     * @return          type name of the component instance
     */
    public static String getComponentTypeFromInstance( ContainerRoot model, String idName )
    {
        for( ContainerNode n : model.getNodes() )
        {
            for( ComponentInstance c : n.getComponents() )
            {
                if( c.getName().equals( idName ))
                {
                    return c.getTypeDefinition().getName();
                }
            }
        }
        return null;
    }

    /**
     * It returns the value of a parameter of a component instance. If the parameter does not exist,
     * it returns null.
     * @param model         the root of the Kevoree model.
     * @param instanceName  the name of the component instance.
     * @param param         the name of the param.
     * @return              the value of the param.
     */
    public static String getParamFromInstance( ContainerRoot model, String instanceName, String param )
    {
        String res = null;

        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                if (instanceName.equals( component.getName() ))
                {
                    if( component.getDictionary() != null )
                    {
                        return component.getDictionary().findValuesByID( param ).getValue();
                    }
                }
            }
        }
        return res;
    }

    /**
     * It returns a list of pairs (Parameter name, Parameter value) of the instance.
     * @param model         the root of the Kevoree model.
     * @param instanceName  the neame of the component instance.
     * @return              the names and values of every parameter of the instance.
     */
    public static HashMap<String,String> getAttributesInfo( ContainerRoot model, String instanceName )
    {
        HashMap<String,String> attr = new HashMap();
        for( ContainerNode node : model.getNodes() )
        {
            for( ComponentInstance component : node.getComponents() )
            {
                if( instanceName.equals( component.getName() ))
                {
                    if( component.getDictionary() != null )
                    {
                        List<Value> values = component.getDictionary().getValues();
                        for( Value val : values )
                        {
                            attr.put(val.getName(), val.getValue());
                        }
                    }
                }
            }
        }
        return attr;
    }

    /**
     * It returns all the pairs (Port, Channel) of a component instance.
     * @param model         the root of the Kevoree model.
     * @param instanceName  name of the component instance.
     * @return              pairs (Port, Channel)
     */
    public static HashMap<String, String> getBindingsInfo( ContainerRoot model, String instanceName )
    {
        HashMap<String, String> res = new HashMap<String, String>();

        for( ContainerNode node : model.getNodes() )
        {
            for (ComponentInstance component : node.getComponents() )
            {
                if ( component.getName().equals( instanceName ))
                {
                    int size = component.getProvided().size();
                    for (int i = 0; i < size; ++i)
                    {
                        //System.out.println("Provided: " + component.getProvided().get( i ).getName());
                        res.put( component.getProvided().get( i ).getName(),
                                 component.getProvided().get( i ).getBindings().get( 0 ).getHub().getName() );
                        //int sizeBindings = component.getProvided().get( i ).getBindings().size();
                        /*for ( int j = 0; j < sizeBindings; ++j )
                        {
                            res.put()
                            System.out.println(component.getProvided().get( i ).getBindings().get(j).getHub().getName());
                        }  */
                    }
                    int size2 = component.getRequired().size();
                    for (int i = 0; i < size2; ++i)
                    {
                        //System.out.println("Required: " + component.getRequired().get( i ).getName());
                        res.put( component.getRequired().get( i ).getName(),
                                 component.getRequired().get( i ).getBindings().get( 0 ).getHub().getName() );
                        //int sizeBindings = component.getRequired().get( i ).getBindings().size();
                        /*for ( int j = 0; j < sizeBindings; ++j )
                        {
                            System.out.println(component.getRequired().get( i ).getBindings().get(j).getHub().getName());
                        } */
                    }
                }
            }
        }

        return res;
    }

    /**
     * It returns the number of component instances of a given component type that is running.
     * @param model         the root of the Kevoree model.
     * @param componentType the type of the component instance
     * @return              number of component instances.
     */
    public static long getNumberOfComponents( ContainerRoot model, String componentType ) {
        long nComponents = 0;
        for (ContainerNode node : model.getNodes())
        {
            for (ComponentInstance component : node.getComponents())
            {
                if (componentType.equals(component.getTypeDefinition().getName()))
                {
                    ++nComponents;
                }
            }
        }
        return nComponents;
    }

    /**
     * It returns whether a given component instance is running.
     * @param model         the root of the Kevoree model.
     * @param instanceName  the name of the component instance.
     * @return              true if the component instance is running; false otherwise.
     */
    public static boolean isInTheModel( ContainerRoot model, String instanceName )
    {
        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                if (instanceName.equals(component.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * It returns a list of all the trustees of a given trustor running on a node.
     * @param model     the root of the Kevoree model.
     * @param context   the context of the trust relationship.
     * @param nodeName  the name of the node where the trustee may be running.
     * @param idTrustor the unique identifier of the trustor component instance.
     * @return  a list of names of component instances.
     */
    public static List<String> getTrusteeInstanceName( ContainerRoot model, String context, String nodeName, String idTrustor ) {

        List<String> components = new ArrayList<String>();
        for (ContainerNode node : model.getNodes()) {
            if (node.getName().equals(nodeName)) {
                for (ComponentInstance component : node.getComponents()) {
                    if (    !idTrustor.equals( component.getName() ) &&
                            component.getDictionary() != null &&
                            component.getDictionary() != null) {
                        if (("trustee".equals(component.getDictionary().findValuesByID("role").getValue()) ||
                                "both".equals(component.getDictionary().findValuesByID("role").getValue())) &&
                                context.equals(component.getDictionary().findValuesByID("trustContext").getValue())) {
                            components.add(component.getName());
                        }
                    }
                }
            }
        }
        return components;
    }

    //It returns a list with the names of all trustee instances running on every node in the model
    //The trustee instances must have the same context as "context"
    // Node 0 - compInst1, compInst2, ...
    // Node 1 - compInst45, compInst46

    /**
     * It returns a list of all the trustees of a given trustor together with the node where they are running.
     * @param model     the root of the Kevoree model.
     * @param context   the context of the trust relationship.
     * @param idTrustor the unique identifier of the trustor component instance.
     * @return          a list of all the trustee component instance names along with the node where they are running
     *                  E.g.
     *                  (
     *                   Node 0 - compInst1, compInst2
     *                   Node 1 - compInst5, compInst50
     *                   etc.
     *                  )
     */
    public static HashMap<String, List<String>> getTrusteesInstanceName( ContainerRoot model, String context, String idTrustor ) {

        HashMap<String, List<String>> components = new HashMap<String, List<String>>();
        List<String> componentsOnNode = new ArrayList<String>();

        for (ContainerNode node : model.getNodes()) {
            componentsOnNode.clear();
            componentsOnNode = getTrusteeInstanceName(model, context, node.getName(), idTrustor);
            if (componentsOnNode.size() > 0)  {
                components.put(node.getName(), new ArrayList<String>(componentsOnNode));
            }
        }

        return components;
    }

    //It returns the names of the components instances that have a port with name portName
    //The third argument indicates the name of the component that called this method. This is to ensure
    //that we don't consider it
    /*public static List<String> componentOnPort(ContainerRoot model, String portName, String componentName) {

        List<String> componentsList = new ArrayList<String>();
        //System.out.println("Looking for all the ports of component " + componentName);
        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                for (Port port : component.getProvided()) {
                    if (portName.equals (port.getPortTypeRef().getName()) &&
                            !component.getName().equals(componentName)) {
                        componentsList.add(component.getName());
                    }
                }
                for (Port port : component.getRequired()) {
                    if (portName.equals (port.getPortTypeRef().getName()) &&
                            !component.getName().equals(componentName)) {
                        componentsList.add(component.getName());
                    }
                }
            }
        }
        return componentsList;
    }  */


    //It returns the name of the binding of the port "portName" of component instance "componentName"
    /*private static String getBindingName(ContainerRoot model, String portName, String componentName) {

        String res = null;

        //List<String> bindings = new ArrayList<String>();
        //System.out.println("Looking for all the bindings of " + portName + " of component " + componentName);
        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                if (component.getName().equals(componentName)) {
                    for (Port port : component.getProvided()) {
                        //System.out.println("Provided ports for " + component.getName() + ": " + port.getPortTypeRef().getName());
                        if (port.getPortTypeRef().getName().equals(portName)) {
                            for (MBinding binding : port.getBindings()) {
                                //System.out.println("Binding: " + binding.getHub().getName());
                                //First we look for the binding of "text entered"
                                res = binding.getHub().getName();
                                //bindings.add( binding.getHub().getName() );
                            }
                        }
                    }

                    for (Port port : component.getRequired()) {
                        //System.out.println("Required ports for " + component.getName() + ": " + port.getPortTypeRef().getName());

                        if (port.getPortTypeRef().getName().equals(portName)) {
                            for (MBinding binding : port.getBindings()) {
                                //System.out.println("Binding: " + binding.getHub().getName());

                                //First we look for the binding of "text entered"
                                res = binding.getHub().getName();
                                //bindings.add( binding.getHub().getName() );
                            }
                        }
                    }

                }

            }
        }

        return res;

    }  */
    //It returns the name of the component instance that is bounded to the port with name portName, and ignoring the
    //component instance with name componentName
    /*public static Set<String> getComponentBindedToPort(ContainerRoot model, String portName, String componentName) {

        //String componentRes = null;
        Set<String> components = new HashSet<String>();

        //We get the name of the binding of the port for component
        String sourceBinding = getBindingName(model, portName, componentName);
        //List<String> sourceBindings = getBindingName( model, portName, componentName );

        //Now, we search for any binding that is equal to the aforementioned binding (but which is not from componentName)
        //System.out.println("Looking for all the bindings of other components with name " + sourceBinding);
        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                if (!component.getName().equals(componentName)) {
                    for (Port port : component.getProvided()) {
                        for (MBinding binding : port.getBindings()) {
                            if (binding.getHub().getName().equals(sourceBinding)) {
                                  //componentRes = component.getName();
                                components.add( component.getName() );
                            }
                        }
                    }
                    for (Port port : component.getRequired()) {
                        for (MBinding binding : port.getBindings()) {
                            if (binding.getHub().getName().equals(sourceBinding)) {
                                //componentRes = component.getName();
                                components.add( component.getName() );
                            }
                        }
                    }
                }
            }
        }
        return components;
    }*/


    /*public static List<String> componentOnPortAndBinding(ContainerRoot model, String componentName) { //String portName, String componentName) {

        List<String> componentsList = new ArrayList<String>();
        System.out.println("Looking for all the ports of component " + componentName);
        for (ContainerNode node : model.getNodes()) {
            for (ComponentInstance component : node.getComponents()) {
                for (Port port : component.getProvided()) {
                    System.out.println("Bindings for port provided " + port.getPortTypeRef().getName());
                    for (MBinding binding : port.getBindings()) {
                        System.out.println(binding.getHub().getName());
                    }
                }
                for (Port port : component.getRequired()) {
                    System.out.println("Bindings for port required " + port.getPortTypeRef().getName());
                    for (MBinding binding : port.getBindings()) {
                        System.out.println(binding.getHub().getName());
                    }
                }
            }

        }
        return componentsList;
    }      */
}
