package kevoree.trCommon;

/**
 * A 3D Tuple to store three values with possibly different types.
 * @param <X>  type of the first value.
 * @param <Y>  type of the second value.
 * @param <Z>  type of the third value.
 */
public final class Tuple<X, Y, Z> {

    /**
     * First value.
     */
    public final X x;

    /**
     * Second value.
     */
    public final Y y;

    /**
     * Third value.
     */
    public final Z z;

    /**
     * Constructor
     * @param x the first value.
     * @param y the second value.
     * @param z the third value.
     */
    public Tuple( X x, Y y, Z z )
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
