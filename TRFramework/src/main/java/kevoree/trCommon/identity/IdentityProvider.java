package kevoree.trCommon.identity;

import java.util.Random;

/**
 * Created by Francisco Moyano on 21/11/14.
 *
 * This static class offers a simple identity provider for Kevoree components.
 * This class can be used by script-related classes when they need to create
 * new instance names for new components.
 */
public final class IdentityProvider {

    /**
     * This method returns a uid for a component running on a node.
     * @param compType  component type.
     * @param node      node where the component is running.
     * @return          a new uid.
     */
    public static String getIdentity( String compType, String node )
    {
        String instanceName = compType.substring( 0, compType.length() / 3 );
        Random rn = new Random();
        int i = rn.nextInt( 500 ) + 1;
        String num = String.valueOf( i );
        return instanceName + num + "@" + node;
    }
}
