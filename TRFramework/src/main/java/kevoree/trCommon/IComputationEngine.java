package kevoree.trCommon;

/**
 * Interface to be implemented by reputation and trust engines.
 * @param <T>   type of the result yielded by the engine.
 */
public interface IComputationEngine<T> {
    /**
     * It computes a trust or reputation value.
     * @param context   context of trust or reputation.
     * @param idTarget  target of the trust or reputation computation.
     * @param idSource  source that initiates the trust or reputation computation.
     * @return          new trust or reputation value.
     */
    public T compute( String context, String idTarget, String idSource );
}
