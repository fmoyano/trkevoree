package kevoree.appCentralRep;

import kevoree.appUtilCommon.ConsolePanel;
import kevoree.appUtilCommon.TextTypedLocallyInterface;
import kevoree.reputationAPI.CentralKevReputableEntity;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.ModelService;
import org.kevoree.api.Port;
import org.kevoree.api.handler.ModelListener;
import org.kevoree.api.handler.UpdateContext;
import org.kevoree.library.java.core.console.ConsoleFrame;
import org.kevoree.library.java.core.console.TabbedConsoleFrame;
import org.kevoree.log.Log;

import javax.swing.*;
import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * Offers a Graphical frame where input text is displayed and where text can also be typed in.
 * Created with IntelliJ IDEA.
 * User: gregory.nain
 * Date: 02/12/2013
 * Time: 10:47
 */
@ComponentType
public class CentralReputationAwareConsole extends CentralKevReputableEntity<String> implements TextTypedLocallyInterface {

    private ConsolePanel<CentralReputationAwareConsole> thisConsole;
    private ConsoleFrame standaloneFrame;

    @Param(defaultValue = "true")
    protected Boolean showInTab = true;

    @Output
    protected Port textEntered;

    @KevoreeInject
    protected Context cmpContext;

    @KevoreeInject
    private ModelService model;

    /**
     * Uniquely identifies the console in tabs panels. Also used as title for the Standalone frame.
     */
    private String consoleKey;

    @Start
    public void startConsole()
    {
        super.start();
        //Uniquely identifies the console
        consoleKey = getUID();
        thisConsole = new ConsolePanel( this );
        thisConsole.appendSystem("/***** CONSOLE READY ******/ ");

        if(showInTab) {
            TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    standaloneFrame = new ConsoleFrame(consoleKey);
                    standaloneFrame.init(thisConsole);
                }
            });
        }
    }

    @Stop
    public void stop()
    {
        if(showInTab) {
            TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
            Log.debug("Stopped from TAB");
        } else {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
            }
        }
        standaloneFrame = null;
        thisConsole = null;
        consoleKey = null;
    }


    @Update
    public void update()
    {
        super.update();
        if(showInTab) {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
                standaloneFrame = null;
                consoleKey = getUID();
                TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
            }
        } else {
            if(standaloneFrame == null) {
                TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
                consoleKey = getUID();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        standaloneFrame = new ConsoleFrame(consoleKey);
                        standaloneFrame.init(thisConsole);
                    }
                });
            }
        }
    }

    public void appendSystem(String text)
    {
        thisConsole.appendSystem(text);
    }

    public void textTypedLocally( String text )
    {
        //System.out.println( consoleKey + " sent text at " + new java.sql.Timestamp( Calendar.getInstance().getTime().getTime() ).toString() );
        long currentTime = System.nanoTime();
        System.out.println( "Current time: " + currentTime );
        textEntered.send( getUID() + "."
                        + getBelongingGroup()  + "."
                        + currentTime + "."
                        + text );
    }


    private String lastMessageReceived = null;
    private long timeSent = 0;

    @Input
    public void showText(Object text)
    {
        if( text != null ) {
            String msg = text.toString();
            StringTokenizer st = new StringTokenizer( msg, "." );
            String idTarget = st.nextToken();
            String group = st.nextToken();
            timeSent = Long.valueOf(st.nextToken());
            String message = st.nextToken();
            if( "shit\n".equals( message ) || "fuck\n".equals( message ) )
            {
                makeClaim("CleanWords", "-1", idTarget);
            }
            else
            {
                //thisConsole.appendIncomming( "->" + message );
                makeClaim( "CleanWords", "1", idTarget );
            }
            lastMessageReceived = message;
            requestReputation( idTarget );
        }
    }

    @Override
    public void reputationReceived( String target, String newVal )
    {
        thisConsole.appendIncomming( "->" + lastMessageReceived );

        long timeReceived = System.nanoTime();
        long timeElapsed = timeReceived - timeSent;
        System.out.println( "Time sent: " + timeSent );
        System.out.println( "Time received: " + timeReceived );
        System.out.println( "Elapsed time: " + timeElapsed );
    }

}
