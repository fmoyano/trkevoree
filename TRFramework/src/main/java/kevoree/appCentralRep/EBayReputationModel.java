package kevoree.appCentralRep;

import kevoree.reputationAPI.ReputationManager;
import org.kevoree.annotation.ComponentType;
import org.kevoree.annotation.Start;

import java.util.List;

@ComponentType
public final class EBayReputationModel extends ReputationManager<String> {

    //Here, we could specify an alternative file name for the .policy file
    @Start
    public void start()
    {
        super.start();
    }

    @Override
    public String compute( String context, String idTarget, String idSource ) {

        List<String> claims = getClaimsValues(context, "CleanWords", idTarget);

        float res = 0.0f;

        if (claims != null) {
            if( claims.size() == 0 )
            {
                System.out.println("Calculated reputation of " + idTarget + ". Value by default; " + 1.0f);
                return "1.0"; //Initial reputation
            }
            for (String c : claims) {
                res += Float.valueOf( c );
            }
        } else {
            System.out.println("claims is null...");
        }
        System.out.println("Calculated reputation of " + idTarget + ". Value " + res);
        return String.valueOf(res);
    }
}
