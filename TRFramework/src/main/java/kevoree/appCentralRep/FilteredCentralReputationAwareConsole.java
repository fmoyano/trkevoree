package kevoree.appCentralRep;

import kevoree.appUtilCommon.ConsolePanel;
import kevoree.appUtilCommon.TextTypedLocallyInterface;
import kevoree.reputationAPI.CentralKevReputableEntity;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.Port;
import org.kevoree.library.java.core.console.ConsoleFrame;
import org.kevoree.library.java.core.console.TabbedConsoleFrame;
import org.kevoree.log.Log;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * Created by franciscomoyanolara on 26/11/14.
 */
@ComponentType
public class FilteredCentralReputationAwareConsole extends CentralKevReputableEntity<String> implements TextTypedLocallyInterface {

    private ConsolePanel<FilteredCentralReputationAwareConsole> thisConsole;
    private ConsoleFrame standaloneFrame;

    @Param(defaultValue = "true")
    protected Boolean showInTab = true;

    @Output
    protected Port textEntered;

    @KevoreeInject
    protected Context cmpContext;

    private PrintWriter writer;

    /**
     * Uniquely identifies the console in tabs panels. Also used as title for the Standalone frame.
     */
    private String consoleKey;

    @Start
    public void startConsole()
    {
        super.start();
        //Uniquely identifies the console
        consoleKey = getUID();
        thisConsole = new ConsolePanel( this );
        thisConsole.appendSystem("/***** CONSOLE READY ******/ ");

        try {
            writer = new PrintWriter( "log.txt", "UTF-8" );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(showInTab) {
            TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    standaloneFrame = new ConsoleFrame(consoleKey);
                    standaloneFrame.init(thisConsole);
                }
            });
        }
    }

    @Stop
    public void stop()
    {
        if(showInTab) {
            TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
            Log.debug("Stopped from TAB");
        } else {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
            }
        }
        standaloneFrame = null;
        thisConsole = null;
        consoleKey = null;
    }


    @Update
    public void update()
    {
        super.update();
        if(showInTab) {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
                standaloneFrame = null;
                consoleKey = getUID();
                TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
            }
        } else {
            if(standaloneFrame == null) {
                TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
                consoleKey = getUID();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        standaloneFrame = new ConsoleFrame(consoleKey);
                        standaloneFrame.init(thisConsole);
                    }
                });
            }
        }
    }

    public void appendSystem(String text)
    {
        thisConsole.appendSystem(text);
    }

    public void textTypedLocally( String text )
    {

        if( "fuck\n".equals( text ) || "shit\n".equals( text ) )
        {
            writer.println( text + " " + new java.sql.Timestamp( Calendar.getInstance().getTime().getTime() ) + "\n" );
            writer.flush();
        }
        textEntered.send( getUID() + "."
                + getBelongingGroup()  + "."
                + text );
    }

    @Input
    public void showText(Object text)
    {
        if( text != null ) {
            String msg = text.toString();
            StringTokenizer st = new StringTokenizer( msg, "." );
            String idTarget = st.nextToken();
            String group = st.nextToken();
            String message = st.nextToken();
            if( "shit\n".equals( message ) || "fuck\n".equals( message ) )
            {
                makeClaim("CleanWords", "-1", idTarget);
            }
            else
            {
                thisConsole.appendIncomming( "->" + message );
                makeClaim( "CleanWords", "1", idTarget );
            }

            System.out.println("Last reputation of " + idTarget + " is " + getLastReputation( idTarget ));
            requestReputation( idTarget );
        }
    }

    @Override
    public void reputationReceived( String target, String newVal )
    {
        System.out.println( getUID() + " has received reputation " + newVal + " for " + target );
    }

}
