package kevoree.reputationAPI;

/**
 * Created by Francisco Moyano on 10/11/2014
 *
 * This interface represents all entities capable of issuing reputation claims about other entities.
 */
public interface IClaimSource {
    /**
     * It makes a claim about target entity.
     * @param name      name of the claim.
     * @param value     value of the claim.
     * @param target    target entity.
     */
    public void makeClaim(String name, String value, String target);
}
