package kevoree.reputationAPI;

import kevoree.trCommon.kevReflection.GetHelper;
import kevoree.trCommon.IComputationEngine;
import kevoree.trCommon.Tuple;
import org.kevoree.annotation.*;
import kevoree.trCommon.kevReflection.ScriptEngine;
import org.kevoree.api.ModelService;
import org.kevoree.api.handler.ModelListener;
import org.kevoree.api.handler.UpdateContext;
import reputationmetamodel.*;
import reputationmetamodel.factory.DefaultReputationMetamodelFactory;
import reputationmetamodel.factory.ReputationMetamodelFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Francisco Moyano on 10/11/2014
 *
 * This class represents a reputation manager, which is the central element in a central reputation model.
 * Central Reputable Entities send their claims to the reputation manager and request the reputation manager for
 * reputation values of other entities. Reputation managers use reputation engines to compute reputation, and
 * sub-classes can override the method compute for such purpose.
 *
 * The generic type T represents the type of the reputation value, which must implement the toString() method. Otherwise
 * behaviour is undefined.
 *
 * The class manages a reputation metamodel that contains elements present in most reputation models. Clients can
 * be oblivious about how this metamodel is arranged and managed.
 */
@ComponentType
public class ReputationManager<T> implements IComputationEngine, ModelListener {

    /**
     * Reputation factory of the metamodel.
     */
    private ReputationMetamodelFactory repFactory = null;

    /**
     * Root of the metamodel.
     */
    private ReputationRoot repRoot = null;

    /**
     * Reputation rules engine.
     */
    private ReputationRulesEngine rre = null;

    /**
     * Script engine.
     */
    private ScriptEngine se = null;

    /**
     *  Kevoree model service.
     */
    @KevoreeInject
    private ModelService model;

    /**
     * Method invoked by the Kevoree runtime to start the component instance.
     * @param fileName  file name for the reputation policy file.
     */
    @Start
    public void start( String fileName ) {
        System.out.println("initializing reputation manager");
        repFactory = new DefaultReputationMetamodelFactory();
        repRoot = repFactory.createReputationRoot();
        se = new ScriptEngine( model );
        rre = new ReputationRulesEngine( fileName, se );
        model.registerModelListener( this );
    }

    /**
     *  Method invoked by the Kevoree runtime to start the component instance.
     */
    protected void start()
    {
        start( "RepRules.policy" );
    }

    /**
     * It retrieves all the claim values that are in a particular trust context with a given name and about
     * a particular target.
     * @param context   trust context.
     * @param name      claim name.
     * @param target    uid of the target.
     * @return          a list of claim values.
     */
    protected final List<String> getClaimsValues( String context, String name, String target ) {

        List<String> claims = new ArrayList<String>();
        for ( ReputationStatement rs : repRoot.getStatements() )
        {
            if ( rs.getContext().equals( context ) &&
                    rs.getTarget().getIdTarget().equals( target ))
            {
                for( Claim claim : rs.getClaim() )
                {
                    if( claim.getName().equals( name ))
                    {
                        claims.add( claim.getClaimValue().getValue() );
                    }
                }
            }
        }
        return claims;
    }

    /**
     * It retrieves claim information about a given target, considering the context where the claim took place and its name.
     * @param context   trust context.
     * @param name      claim name.
     * @param target    uid of the target.
     * @return          a list of tuples consisting of the value of the claim, the source entity uid that made the claim, and the
     *                  the time when the claim was created.
     */
    protected final List<Tuple<String,String,String>> getClaimsInfo( String context, String name, String target )
    {
        List<Tuple<String, String, String>> res = new ArrayList<Tuple<String,String,String>>();
        for ( ReputationStatement rs : repRoot.getStatements() )
        {
            if ( rs.getContext().equals( context ) &&
                    rs.getTarget().getIdTarget().equals( target ))
            {
                for( Claim claim : rs.getClaim() )
                {
                    if( claim.getName().equals( name ))
                    {
                        String value = claim.getClaimValue().getValue();
                        String source = rs.getSource().getIdSource();
                        String timeStamp = claim.getClaimValue().getTimeStamp();
                        res.add( new Tuple<String,String,String>( value, source, timeStamp) );
                    }
                }
            }
        }
        return res;
    }

    /**
     * This method retrieves a parameter about a component. Clients can use it for example to retrieve
     * the group to which a component belongs.
     * @param idComponent   uid of the component.
     * @param param         parameter.
     * @return              value of the parameter.
     */
    protected final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@" ).nextToken();
        System.out.println("Calling getParam with param " + param + " and instance " + instance);
        return GetHelper.getParamFromInstance( model.getCurrentModel().getModel(), instance, param );
    }

    /**
     * This method computes a reputation value. It can be overriden by sub-classes to implement different
     * reputation engines.
     * @param context   trust context.
     * @param idTarget  uid of the target component.
     * @param idSource  uid of the component that requested the computation.
     * @return          reputation value.
     */
    public T compute( String context, String idTarget, String idSource )
    {
        return null;
    }
    /****************************METHODS ASSOCIATED TO PORTS******************************************/
    /**
     * This method receives a reputation statement (an extended claim) and stores it in the metamodel.
     * @param rs  reputation statement.
     */
    @Input
    private void receiveClaim(Object rs) {

        String context = null, source = null, target = null;
        long timeStamp = 0;
        ClaimInfo claim = null;
        long id = -1;
        if (rs instanceof ReputationStatementInfo) {
            context = ((ReputationStatementInfo) rs).getContext();
            source =  ((ReputationStatementInfo) rs).getSource();
            target =  ((ReputationStatementInfo) rs).getTarget();
            claim = ((ReputationStatementInfo) rs).getClaim();
            timeStamp = ((ReputationStatementInfo) rs).getTimeStamp();
            id = ((ReputationStatementInfo) rs).getId();
        }

        //We always want to create a new claim (with a new value)
        Claim c = repFactory.createClaim();
        ClaimValue cv = repFactory.createClaimValue();

        //We create the source if it does not exist yet
        Source s = repRoot.findSourcesByID(source);
        if( s == null )
        {
            s = repFactory.createSource();
            s.setIdSource(source);
            repRoot.addSources(s);
        }

        //We fill in the claim
        //The id of a claim is the name of the claim, the target, and the time stamp
        c.setIdClaim( claim.getName() + target + timeStamp );
        c.setName( claim.getName() );
        cv.setValue( claim.getValue().toString() );
        cv.setTimeStamp( String.valueOf(timeStamp) );
        c.setClaimValue( cv );
        repRoot.addClaims(c);

        //We create the target if it does not exist yet
        Target t = repRoot.findTargetsByID(target);
        if ( t == null )
        {
            t = repFactory.createTarget();
            t.setIdTarget(target);
            repRoot.addTargets(t);
        }


        //We create the reputation statement
        ReputationStatement rstatement = repFactory.createReputationStatement();
        rstatement.setIdRepStatement( String.valueOf( id ) );
        rstatement.setContext(context);
        rstatement.setSource(s);
        rstatement.setTarget(t);
        rstatement.addClaim(c);
        repRoot.addStatements(rstatement);
    }

    /**
     * This method computes a reputation given a request. This method in turn calls compute(), which
     * can be overriden by sub-classes to provide reputation engines.
     * @param request   reputation request information (e.g. target UID).
     * @return reputation value.
     * @throws ReputationException
     */
    @Input
    private T computeReputation( Object request ) throws ReputationException
    {
        StringTokenizer st = new StringTokenizer( request.toString(), "." );
        if ( !st.hasMoreTokens() )
        {
            throw new ReputationException( "Request information missing for computing reputation");
        }
        String context = st.nextToken();

        if ( !st.hasMoreTokens() )
        {
            throw new ReputationException( "Request information missing for computing reputation");
        }
        String idTarget = st.nextToken();

        if ( !st.hasMoreTokens() )
        {
            throw new ReputationException( "Request information missing for computing reputation");
        }
        String idSource = st.nextToken();

        boolean reconfigure = false;
        if ( !st.hasMoreTokens() || "true".equals( st.nextToken() ))
        {
            reconfigure = true;
        }

        T res = compute( context, idTarget, idSource );

        if ( reconfigure )
        {
            rre.executeRules( model, idTarget, res.toString() );
        }
        else
        {
            System.out.println("User does not want to reconfigure the system");
        }
        return res;
    }

    @Override
    public boolean preUpdate(UpdateContext context) {
        return true;
    }

    @Override
    public boolean initUpdate(UpdateContext context) {
        return true;
    }

    @Override
    public boolean afterLocalUpdate(UpdateContext context) {
        return true;
    }

    @Override
    public void modelUpdated() {
        System.out.println("MODEL UPDATED AT " + new java.sql.Timestamp( Calendar.getInstance().getTime().getTime() ).toString() );
    }

    @Override
    public void preRollback(UpdateContext context) {

    }

    @Override
    public void postRollback(UpdateContext context) {

    }
    /**********************************************************************/
}
