package kevoree.reputationAPI;

import org.kevoree.annotation.*;
import org.kevoree.api.Callback;
import org.kevoree.api.Context;
import org.kevoree.api.Port;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Francisco Moyano on 10/11/2014
 *
 * This class represents a reputable entity that follows a central reputation model.
 * This class must be inherited by any component that wants to rate other components.
 *
 * The generic types represent the type of the reputation value and the type of the claims.
 * Both types must implement the method toString(); otherwise, behaviour is undefined.
 */
@ComponentType
public class CentralKevReputableEntity<T> implements IClaimSource {

    /**
     * Trust context of the entity.
     */
    @Param(defaultValue = "MyContext")
    private String trustContext;

    /**
     * Group to which the entity belongs.
     */
    @Param(defaultValue = "MyGroup")
    private String group;

    @KevoreeInject
    private Context context;

    /**
     * Port through which the entity sends claims to the manager.
     */
    @Output
    private Port sendClaim;

    /**
     * Port through which the entity request reputation to the manager.
     */
    @Output
    private Port requestReputation;

    /**
     * Simple cache for already-computed reputation values
     */
    private HashMap<String, T> reputationCache;

    /**
     * Unique identifier for the component.
     */
    private String uid;

    /**
     * This method is called by the Kevoree runtime at start-up. It should be explicitly invoked by
     * sub-classes in the first line of their start methods.
     */
    @Start
    public void start()
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();
        System.out.println("initializing central reputable entity");
        reputationCache = new HashMap<String, T>();
    }

    /**
     * This method is called by the Kevoree runtime at each update. It should be explicitly invoked by
     * by sub-classes in the first line of their update methods.
     */
    @Update
    public void update()
    {
        uid = context.getInstanceName() + "@" + context.getNodeName();
    }


    /**
     * This method is called right after a new reputation value for a target has been received,
     * and it stores the value in a cache.
     * @param target    the uid of the target.
     * @param newVal    the new reputation value.
     */
    private void onReputationReceived( String target, T newVal )
    {
        reputationCache.put( target, newVal );
        reputationReceived( target, newVal );
    }

    /**
     * This method is automatically called when a new reputation value is received after having been requested.
     * It can be overriden by sub-classes to act on the new reputation value.
     * @param target    the uid of the component instance about which the reputation is informed.
     * @param newVal    the reputation value.
     */
    protected void reputationReceived( String target, T newVal )
    {
        System.out.println( "Reputation of " + target + " is " + newVal );
    }

    /**
     * This method provides the last computed reputation value for the target. It does not
     * trigger a new reputation computation, so it may return null.
     * @param target    the uid of the target.
     * @return          the last reputation value of the target.
     */
    protected final T getLastReputation( String target )
    {
        return reputationCache.get( target );
    }

    /**
     * This method allows clients to make a claim about a given target.
     * @param name      the name of the claim.
     * @param value     the value of the claim.
     * @param target    the uid of the target component.
     */
    public final void makeClaim( String name, String value, String target )
    {
        sendClaim( new ClaimInfo( name, value ), target );
    }

    /**
     * It returns the group to which the component belongs.
     * @return group.
     */
    protected final String getBelongingGroup()
    {
        return group;
    }

    /**
     * It returns the trust context.
     * @return  trust context.
     */
    protected final String getContext()
    {
        return trustContext;
    }

    /**
     * It returns the unique identifier of the component.
     * @return  unique identifier.
     */
    protected final String getUID()
    {
        return uid;
    }


    /******************** METHODS ASSOCIATED TO PORTS **************************/
    /**
     * This method requests the reputation value of a component. It implicitly assumes that the client
     * wants to reconfigure the system when reputation is computed.
     * @param uidTarget the uid of the component instance.
     */
    protected final void requestReputation( final String uidTarget )
    {
         requestReputation( uidTarget, true );
    }

    /**
     * This method requests the reputation of a component, and explicitly allows the caller to specify
     * whether a reconfiguration of the system should take place afterwards.
     * @param uidTarget     the uid of the component instance.
     * @param reconfigure   whether to reconfigure the system if it is required.
     */
    protected final void requestReputation( final String uidTarget, boolean reconfigure )
    {
        requestReputation.call( trustContext + "." + uidTarget + "." + uid + "." + String.valueOf( reconfigure ),
                new Callback<String>() {
                    @Override
                    public void onSuccess( String result )
                    {
                        onReputationReceived( uidTarget, (T) result );
                    }

                    @Override
                    public void onError(Throwable exception)
                    {
                        System.out.println("Error after requesting reputation");
                    }
                });
    }

    /**
     * This method sends a claim about a target to the reputation manager.
     * @param claim     the claim.
     * @param idTarget  the uid of the component instance.
     */
    private void sendClaim( ClaimInfo claim, String idTarget )
    {
        ReputationStatementInfo rs = new ReputationStatementInfo( getContext(),
                uid,
                claim,
                idTarget,
                Calendar.getInstance().getTime().getTime() );
        sendClaim.send( rs );
    }
    /***************************************************************************/
}
