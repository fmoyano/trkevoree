package kevoree.reputationAPI;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import kevoree.trCommon.Tuple;
import kevoree.trCommon.kevReflection.GetHelper;
import kevoree.trCommon.kevReflection.ScriptEngine;
import org.kevoree.annotation.*;
import org.kevoree.api.*;
import reputationmetamodel.*;
import reputationmetamodel.factory.DefaultReputationMetamodelFactory;
import reputationmetamodel.factory.ReputationMetamodelFactory;

import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: franciscomoyanolara
 * Date: 25/10/13
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
public class DistKevReputableEntity<T> implements IClaimSource {

    /**
     * Trust context of the entity.
     */
    @Param(defaultValue = "MyContext")
    private String trustContext;

    /**
     * Group to which the entity belongs.
     */
    @Param(defaultValue = "MyValue")
    private String group;


    @KevoreeInject
    private Context ctx;

    @KevoreeInject
    private ModelService model;

    /**
     * Port through which the entity requests claims to other entities.
     */
    @Output
    private Port requestClaim;


    /**
     * Port through which the entity informs other entities about the new reputation value of a given entity.
     */
    @Output
    private Port newReputationValue;


    /**
     * Unique identifier for the component.
     */
    private String uid;

    /**
     * Fields for reputation metamodel management
     */
    private ReputationMetamodelFactory repFactory = null;
    private ReputationRoot repRoot = null;

    /**
     * Instance of the reputation engine used by the entity
     */
    private ReputationEngine<T> reputationEngine;

    /**
     * Instance of the rules engine used by the entity for reconfiguration purposes
     */
    private ReputationRulesEngine rre;

    /**
     * Instance of the script engine to execute the reconfiguration ules
     */
    private ScriptEngine se;

    /**
     * Simple cache for already-computed reputation values
     */
    private HashMap<String, String> reputationCache;

    private long numberOfDistributedEntities;

    /**
     * Unique identifier of reputation statements
     */
    private static long id = 0;

    /**
     * List with the reputation statements made by this entity
     */
    private List<ReputationStatementInfo> listStatements;


    /**
     * This method is called by the Kevoree runtime at start-up. It should be explicitly invoked by
     * sub-classes in the first line of their start methods.
     * @param repEngine a reputation engine that the entity will used to compute reputation.
     * @param fileName  the name of the file that contains the reconfiguration policy
     */
    @Start
    public void start( ReputationEngine repEngine, String fileName )
    {
        repFactory = new DefaultReputationMetamodelFactory();
        repRoot = repFactory.createReputationRoot();
        uid = ctx.getInstanceName() + "@" + ctx.getNodeName();
        reputationEngine = repEngine;
        se = new ScriptEngine( model );
        rre = new ReputationRulesEngine( fileName, se );
        reputationCache = new HashMap();
        numberOfDistributedEntities = 0;
        listStatements = new ArrayList();
    }

    /**
     * This method is called by the Kevoree runtime at start-up. It should be explicitly invoked by
     * sub-classes in the first line of their start methods. It implicitly assumes that the reputation policy file
     * is called RepRules.policy.
     * @param repEngine a reputation engine that the entity will used to compute reputation.
     */
    protected void start( ReputationEngine repEngine )
    {
        start( repEngine, "RepRules.policy" );
    }

    /**
     * This method is called by the Kevoree runtime at each update. It should be explicitly invoked by
     * by sub-classes in the first line of their update methods.
     */
    @Update
    public void update()
    {
        uid = ctx.getInstanceName() + "@" + ctx.getNodeName();
    }

    /**
     * This method issues a claim.
     * @param name      the name of the claim.
     * @param value     the value of the claim.
     * @param target    the target entity.
     */
    public final void makeClaim(String name, String value, String target) {
        String context = trustContext;
        String source = uid;

        //We always want to create a new claim (with a new value)
        Claim c = repFactory.createClaim();
        ClaimValue cv = repFactory.createClaimValue();

        //We create the source if it does not exist yet
        Source s = repRoot.findSourcesByID(source);
        if (s == null) {
            s = repFactory.createSource();
            s.setIdSource(source);
            repRoot.addSources(s);
        }

        //We fill in the claim
        //The id of a claim is the name of the claim, the target, and the time stamp
        long timeStamp = Calendar.getInstance().getTime().getTime();
        String ts = String.valueOf( timeStamp );
        c.setIdClaim(name + target + ts );
        c.setName( name );
        cv.setValue( value.toString() );
        cv.setTimeStamp( ts );
        c.setClaimValue(cv);
        repRoot.addClaims(c);

        //We create the target if it does not exist yet
        Target t = repRoot.findTargetsByID(target);
        if (t == null) {
            t = repFactory.createTarget();
            t.setIdTarget(target);
            repRoot.addTargets(t);
        }

        //We create the reputation statement
        ReputationStatement rstatement = repFactory.createReputationStatement();
        rstatement.setIdRepStatement(String.valueOf(id));
        rstatement.setContext(context);
        rstatement.setSource(s);
        rstatement.setTarget(t);
        rstatement.addClaim(c);
        repRoot.addStatements(rstatement);

        ++id;
    }

    /**
     * It returns the unique identifier of the component.
     * @return  the unique identifier.
     */
    public final String getUID()
    {
        return uid;
    }

    /**
     * It returns the group to which the component belongs.
     * @return the group identifier.
     */
    public final String getBelongingGroup() {
        return group;
    }

    /**
     * It returns the trust context.
     * @return  the trust context.
     */
    public final String getContext() {
        return trustContext;
    }

    /**
     * It returns a String representing the value of a parameter of the entity.
     * @param idComponent   the unique identifier of the component.
     * @param param         the name of the parameter.
     * @return  the value of the parameter.
     */
    public final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@").nextToken();
        return GetHelper.getParamFromInstance( model.getCurrentModel().getModel(),
                                                instance,
                                                param );
    }

    /**
     * It returns a String that contains all the reputation statements about a target in a context.
     * @param ctx       the context of the reputation.
     * @param target    the target entity.
     * @return          a list of reputation statements about a target.
     */
    private String sendClaims ( String ctx, String target ) {

        JsonArray outerArray = new JsonArray();

        for ( ReputationStatement rs : repRoot.getStatements() )
        {
            if ( rs.getContext().equals( trustContext ) )
            {
                for( Claim claim : rs.getClaim() )
                {
                    JsonArray nestedArray = new JsonArray().add( trustContext ).
                                                            add( rs.getSource().getIdSource()).
                                                            add( claim.getName() ).
                                                            add( claim.getClaimValue().getValue()).
                                                            add( rs.getTarget().getIdTarget() ).
                                                            add( claim.getClaimValue().getTimeStamp());
                    outerArray.add( nestedArray );
                }
            }
        }
        return outerArray.toString();
    }

    /**
     * It returns a String that contains all the reputation statements issued by a sender about a target in a context.
     * @param ctx       the context of the reputation.
     * @param target    the unique identifier of the target entity.
     * @param sender    the entity that requests the information.
     * @return          a list of reputation statements.
     */
    private String sendClaims ( String ctx, String target, String sender ) {

        //PREGUNTA: parece que ahora se envían todas las claims, aunque no sean del target, ¿por qué?
        //RESPUESTA: porque es necesario para implementar REGRET (necesita acceder a RSs de entidades que están en el mismo grupo)
        JsonArray res = new JsonArray();
        for ( ReputationStatement rs : repRoot.getStatements() )
        {
            if ( rs.getContext().equals( trustContext ) )
            {
                for( Claim claim : rs.getClaim() )
                {
                    JsonArray rsInfo = new JsonArray().add( trustContext ).
                            add( rs.getSource().getIdSource()).
                            add( claim.getName() ).
                            add( claim.getClaimValue().getValue()).
                            add( rs.getTarget().getIdTarget() ).
                            add(claim.getClaimValue().getTimeStamp());
                    res.add( rsInfo );
                }
            }
        }
        return res.toString();
    }

    /******************** METHODS ASSOCIATED TO PORTS *******************/
    /**
     * This method requests claims to the rest of entities about a given target.
     * @param uidTarget    the unique identifier of the target entity.
     * @param reconfigure  it specifies whether the caller wants to reconfigure the system in case it is needed.
     */
    private void requestClaims( final String uidTarget, final boolean reconfigure )
    {
        //call sendClaims under the hood
        requestClaim.call( trustContext + "." + uidTarget + "." + uid,
                new Callback<String>() {
                    @Override
                    public void onSuccess( String result )
                    {
                        if ( result != null )
                        {
                            onClaimsReceived( uidTarget, result, reconfigure );
                        }
                    }

                    @Override
                    public void onError( Throwable exception ) {
                        System.out.println("On error: " + exception.getMessage() );
                    }
                });
    }

    /**
     * This method send the claims about a target entity. It is associated to an input port,
     * so this method first receives a request and then sends the claims back.
     * @param request   the object encapsulating the request information.
     * @return          a String representing all the reputation statements.
     */
    @Input
    private final String sendClaims( Object request )
    {
        StringTokenizer st = new StringTokenizer( request.toString(), ".");
        String context = st.nextToken();
        String target = st.nextToken();
        String sender = st.nextToken();
        //It doesn't make sense that the target sends its claims
        if ( target.equals( uid ))
        {
             return null;
        }
        return sendClaims( context, target, sender );
    }

    /**
     * This method is invoked by other entities in order to inform the current entity about an update of an entity reputation.
     * @param p  the payload of the request.
     */
    @Input
    protected final void receiveReputationValue( Object p )
    {
        String payload = p.toString();
        StringTokenizer st = new StringTokenizer( payload, "$" );
        String target = st.nextToken();
        String repValue = st.nextToken();
        String source = st.nextToken();

        reputationCache.put( target, repValue );
    }
    /********************************************************/

    /**
     * This method requests the reputation of a component, and explicitly allows the caller to specify
     * whether a reconfiguration of the system should take place afterwards.
     * @param uidTarget     the unique identifier of the component instance.
     * @param reconfigure   it tells whether to reconfigure the system.
     */
    protected final void requestReputation( String uidTarget, boolean reconfigure )
    {
        requestClaims(uidTarget, reconfigure);
    }

    /**
     * This method requests the reputation value of a component. It implicitly assumes that the client
     * wants to reconfigure the system when reputation is computed.
     * @param uidTarget the unique identifier of the component instance.
     */
    protected final void requestReputation( String uidTarget )
    {
        requestReputation(uidTarget, true);
    }

    /**
     * This method is automatically called when a new reputation value is received after having being requested.
     * It can be overriden by sub-classes to act on the new reputation value.
     * @param uidTarget     the unique identifier of the component instance about which the reputation is informed.
     * @param newVal        the reputation value.
     */
    protected void reputationReceived( String uidTarget, T newVal )
    {
        System.out.println("Reputation of  " + uidTarget + "  is " + newVal.toString());
    }

    /**
     * This method provides the last computed reputation value for the target. It does not
     * trigger a new reputation computation, so it may return null.
     * @param uidTarget     the unique identifier of the target.
     * @return              the last computed reputation value of the target.
     */
    public final String getLastReputation( String uidTarget )
    {
        return reputationCache.get( uidTarget );
    }

    /**
     * This method is called when the current entity receives the claims that it requested to other entities.
     * @param uidTarget     the unique identifier of the target entity of the claim.
     * @param result        the list of claims, encoded as a String.
     * @param reconfigure   it tells wether client wants to reconfigure if it is needed, after computing reputation.
     */
    private void onClaimsReceived( String uidTarget, String result, boolean reconfigure )
    {

        System.out.println( uid + " has received claims about " + uidTarget );

        //This entity has received claims from another entity
        ++numberOfDistributedEntities;

        //Check how many distributed entities there are currently in the model
        String myComponentType = GetHelper.getComponentTypeFromInstance( model.getCurrentModel().getModel(), ctx.getInstanceName() );
        long numberEntitities = GetHelper.getNumberOfComponents(
                                                            model.getCurrentModel().getModel(),
                                                            myComponentType
                                                            );

        JsonArray reputationStatements = JsonArray.readFrom( result );
        for( int i = 0; i < reputationStatements.size(); ++i )
        {
                listStatements.add(
                    new ReputationStatementInfo(
                            reputationStatements.get(i).asArray().get(0).asString(), //context
                            reputationStatements.get(i).asArray().get(1).asString(), //idSource
                            new ClaimInfo(reputationStatements.get(i).asArray().get(2).asString(), //claim name
                                          reputationStatements.get(i).asArray().get(3).asString() //claim value
                            ),
                            reputationStatements.get(i).asArray().get(4).asString(), //idTarget
                            Long.valueOf(reputationStatements.get(i).asArray().get(5).asString()) //timestamp

                    )

            );
        }

        if( numberOfDistributedEntities == numberEntitities - 2 ) //-2 in order to not count itself or the target
        {
            List<ReputationStatement> myStatements = repRoot.getStatements();
            for (ReputationStatement rs : myStatements) {
                if (trustContext.equals(rs.getContext())) {
                    for (Claim claim : rs.getClaim()) {
                        listStatements.add(new ReputationStatementInfo(
                                trustContext,
                                rs.getSource().getIdSource(),
                                new ClaimInfo(claim.getName(), claim.getClaimValue().getValue()),
                                rs.getTarget().getIdTarget(),
                                Long.parseLong(claim.getClaimValue().getTimeStamp())));
                    }

                }
            }

            //Now with all the claims
            computeReputation(uidTarget, reconfigure, listStatements);

            //reset
            numberOfDistributedEntities = 0;
            listStatements = new ArrayList();
        }
    }

    /**
     * Computes the reputation about a target entity.
     * @param idTarget      the unique identifer of the target entity.
     * @param reconfigure   it specifies whether, if it is necessary, a reconfiguration should be performed.
     * @param rsInfo        a list of reputation statements.
     */
    private void computeReputation( String idTarget, boolean reconfigure, List<ReputationStatementInfo> rsInfo )
    {
        //System.out.println("Calling reputation engine");
        T res = reputationEngine.compute( model, trustContext, idTarget, uid, rsInfo, this );

        if ( reconfigure )
        {
            //System.out.println("We have to execute rules");
            rre.executeRules( model, idTarget, res.toString() );
        }
        //we cache the last reputation value
        reputationCache.put( idTarget, res.toString() );

        //and inform the rest of entities
        newReputationValue.send( idTarget + "$" + res.toString()  + "$" + uid );

        //System.out.println("Calling reputation received");
        reputationReceived(idTarget, res);
    }
}
