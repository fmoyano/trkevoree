package kevoree.reputationAPI;

/**
 * Created by franciscomoyanolara on 25/11/14.
 *
 * This class signals an exception belonging to any reputation-related operation.
 */
final class ReputationException extends Exception {

    ReputationException()
    {
        super();
    }

    ReputationException( String message )
    {
        super( message );
    }
}
