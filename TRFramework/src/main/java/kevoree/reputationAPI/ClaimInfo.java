package kevoree.reputationAPI;

/**
 * Created by Francisco Moyano on 10/11/2014
 *
 * This class represents a claim.
 */
public final class ClaimInfo {

    /**
     * Name of the claim.
     */
    private String name;

    /**
     * Value of the claim.
     */
    private String value;

    /**
     * Constructor takes a name and a value.
     * @param name      name of the new claim.
     * @param value     value of the new claim.
     */
    ClaimInfo( String name, String value )
    {
        this.name = name;
        this.value = value;
    }

    /**
     * It returns the name of the claim.
     * @return  name of the claim
     */
    public String getName()
    {
        return name;
    }

    /**
     * It returns the value of the claim.
     * @return  value of the claim.
     */
    public String getValue() {
        return value;
    }

}
