package kevoree.reputationAPI;

import kevoree.trCommon.kevReflection.GetHelper;
import kevoree.trCommon.kevReflection.ScriptEngine;
import org.kevoree.ContainerRoot;
import org.kevoree.api.ModelService;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Francisco Moyano on 17/11/14.
 *
 * This class encapsulates the rules for reputation-based self-adaptation. It reads
 * rules from a file, and calls a ScriptEngine instance to actually execute the rules
 * in Kevscript.
 */
final class ReputationRulesEngine {

    /**
     * File reader for the file.
     */
    private FileReader fi;

    /**
     * Path for reading the file.
     */
    private Path path;

    /**
     * ScriptEngine instance.
     */
    private ScriptEngine se;


    /**
     *
     * @param fileName  name of the file with the reconfiguration rules.
     * @param se        ScriptEngine instance.
     */
    ReputationRulesEngine( String fileName, ScriptEngine se ) {
        try {
            fi = new FileReader( fileName );
            path = Paths.get( fileName );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.se = se;
    }

    /**
     * This method execute the rules read from the file. Given that the rules are stated (for now)
     * for component types, we need to pass in the component instance onto which the action is to be applied.
     * @param model             root of the Kevoree model.
     * @param idTarget          uid of the component on which the rule applies.
     * @param reputationValue   reputation value of the idTarget component.
     */
    void executeRules( ModelService model, String idTarget, String reputationValue )
    {
        List<String> rules = new ArrayList<String>();
        try {
            rules = Files.readAllLines( path, StandardCharsets.UTF_8 );
        } catch (IOException e) {
            e.printStackTrace();
        }
        for( String rule : rules )
        {
            processRule( rule, model.getCurrentModel().getModel(), idTarget, reputationValue );
        }
    }

    /**
     * This utility method process a rule.
     * @param rule              rule to be processed.
     * @param model             root of the Kevoree model.
     * @param idTarget          uid of the component on which the rule applies.
     * @param reputationValue   reputation value of the idTarget component.
     */
    private void processRule( String rule, ContainerRoot model, String idTarget, String reputationValue )
    {
        StringTokenizer st1 = new StringTokenizer( idTarget, "@");
        String instanceName = st1.nextToken();
        String componentType = GetHelper.getComponentTypeFromInstance(model, instanceName);

        StringTokenizer st = new StringTokenizer( rule );
        String element = st.nextToken();
        if( element.equals( componentType ))
        {
            String condition = st.nextToken();
            char booleanValue = condition.charAt(0);
            String value = condition.substring(1);
            if( conditionSatisfied( booleanValue, value, reputationValue ))
            {
                String action = st.nextToken();
                List<String> args = new ArrayList<String>();
                while ( st.hasMoreTokens() )
                {
                    args.add( st.nextToken() );
                }
                se.execute( action, idTarget, args );
            }
        }
    }

    /**
     * This utility method determines whether a condition is satisfied. It determines whether lhs and
     * rhs fulfils the boolean condition represented by the boolean operator.
     * @param booleanOperator      it represents a boolean operator.
     * @param lhs                  left-hand side value of the operator.
     * @param rhs                  right-hand side value of the operator.
     * @return
     */
    private boolean conditionSatisfied( char booleanOperator, String lhs, String rhs )
    {
        float val = Float.parseFloat( lhs );
        float repVal = Float.parseFloat( rhs );
        if( '<' == booleanOperator )
        {
            return val > repVal;
        }
        else if ( '>' == booleanOperator )
        {
            return val < repVal;
        }
        else
        {
            return repVal == val;
        }

    }
}
