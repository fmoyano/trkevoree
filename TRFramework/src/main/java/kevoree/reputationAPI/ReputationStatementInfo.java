package kevoree.reputationAPI;

/**
 * Created by Francisco Moyano on 10/11/2014
 *
 * This class represents a reputation statement, which is a claim extended with further information.
 */
public final class ReputationStatementInfo {

    /**
     * Unique id for the statement.
     */
    private static long id = 0;

    /**
     * Trust context of the statement.
     */
    private String context;

    /**
     * Source entity of the statement.
     */
    private String source;

    /**
     * Claim of the statement.
     */
    private ClaimInfo claim;

    /**
     * Target entity of the statement.
     */
    private String target;

    /**
     * Time information about the statement.
     */
    private long timeStamp;


    /**
     * Constructor
     * @param ctx       context of the reputation statement.
     * @param source    entity that issues the reputation statement.
     * @param claim     claim of the reputation statement.
     * @param target    entity about which the reputation statement claim is.
     * @param timestamp time stamp of the reputation statement.
     */
    ReputationStatementInfo( String ctx, String source, ClaimInfo claim, String target, long timestamp )
    {
        ++id;
        this.context = ctx;
        this.source = source;
        this.claim = claim;
        this.target = target;
        this.timeStamp = timestamp;
    }

    /**
     * Internal unique identifier of the reputation statement.
     * @return  unique identifier.
     */
    long getId()
    {
        return id;
    }

    /**
     * Getter of the context.
     * @return  context of the reputation statement.
     */
    public String getContext()
    {
        return context;
    }

    /**
     * Getter of the source.
     * @return  source of the reputation statement.
     */
    public String getSource()
    {
        return source;
    }

    /**
     * Getter of the claim.
     * @return  claim of the reputation statement.
     */
    public ClaimInfo getClaim()
    {
       return claim;
    }

    /**
     * Getter of the target.
     * @return  target of the reputation statement.
     */
    public String getTarget()
    {
        return target;
    }

    /**
     * Getter of the time stamp.
     * @return  time stamp of the reputation statement.
     */
    public long getTimeStamp()
    {
        return timeStamp;
    }
}
