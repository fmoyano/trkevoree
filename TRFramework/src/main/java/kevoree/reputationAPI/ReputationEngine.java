package kevoree.reputationAPI;

import kevoree.trCommon.IComputationEngine;
import kevoree.trCommon.kevReflection.GetHelper;
import org.kevoree.api.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This class represents a reputation engine that can be used by distributed reputable entities in order to describe how to compute reputation.
 * @param <T>   type of the reputation value.
 */
public class ReputationEngine<T> implements IComputationEngine<T> {

    /**
     * Reputation statements that the engine can used for its computations.
     */
    private List<ReputationStatementInfo> reputationStatements;

    /**
     * Access to the reflection layer of Kevoree.
     */
    private ModelService model;

    /**
     * Reputable entity that owns this engine.
     */
    private DistKevReputableEntity<T> ownerEntity;

    /**
     * This method retrieves all the claims about a given target that were issued by another entity.
     * @param name      the name of the claim.
     * @param idSource  the unique identifier of the entity that issued the claim.
     * @param idTarget  the unique identifier of the target entity.
     * @return  a list of reputation statements.
     */
    protected final List<ReputationStatementInfo> getClaims( String name, String idSource, String idTarget )
    {

        List<ReputationStatementInfo> claims = new ArrayList();

        for( ReputationStatementInfo cInfo : reputationStatements )
        {
            if( name.equals(cInfo.getClaim().getName()) && cInfo.getSource().equals( idSource ) && cInfo.getTarget().equals( idTarget ))
            {
                claims.add( cInfo );
            }
        }
        return claims;
    }

    /**
     * This method retrieves all the claims about a given target.
     * @param name      the name of the claim.
     * @param idTarget  the unique identifier of the target entity.
     * @return  a list of reputation statements.
     */
    protected final List<ReputationStatementInfo> getClaimsAboutTarget( String name, String idTarget )
    {
        List<ReputationStatementInfo> claims = new ArrayList();

        for( ReputationStatementInfo cInfo : reputationStatements )
        {
            if( name.equals(cInfo.getClaim().getName()) &&  cInfo.getTarget().equals( idTarget ))
            {
                claims.add( cInfo );
            }
        }
        return claims;
    }

    /**
     * This method retrieves all the claims issued by a give entity.
     * @param name      the name of the claim.
     * @param idSource  the unique identifier of the source entity.
     * @return  a list of reputation statements.
     */
    protected final List<ReputationStatementInfo> getClaimsFromSource( String name, String idSource )
    {
        List<ReputationStatementInfo> claims = new ArrayList();

        for( ReputationStatementInfo cInfo : reputationStatements )
        {
            if( name.equals(cInfo.getClaim().getName()) && cInfo.getSource().equals( idSource ))
            {
                claims.add( cInfo );
            }
        }
        return claims;
    }

    /**
     * This method retrieves a parameter about a component. Clients can use it for example to retrieve
     * the group to which a component belongs.
     * @param idComponent   the uid of the component.
     * @param param         the name of the parameter.
     * @return              the value of the parameter.
     */
    protected final String getParam( String idComponent, String param )
    {
        String instance = new StringTokenizer( idComponent, "@" ).nextToken();
        return GetHelper.getParamFromInstance(model.getCurrentModel().getModel(), instance, param);
    }

    /**
     * This method is called by reputatable entities to compute reputation. It is not visible to client code outside the package.
     * @param model     the root of the Kevoree model.
     * @param context   the context of the reputation.
     * @param idTarget  the unique identifier of the target entity.
     * @param idSource  the unique identifier of the source entity.
     * @param rs        a list of reputation statements that can be used for the reputation update.
     * @param entity    the distributed reputable entity that is using this reputation engine.
     * @return          updated reputation value.
     */
    final T compute( ModelService model, String context, String idTarget, String idSource, List<ReputationStatementInfo> rs, DistKevReputableEntity entity )
    {
        this.ownerEntity = entity;
        this.reputationStatements = rs;
        this.model = model;
        T res = compute( context, idTarget, idSource );
        return res;
    }

    /**
     * This method retrieves the last reputation value associated to an entity.
     * @param idEntity  the unique identifier of the entity.
     * @return  the reputation value.
     */
    protected final String getLastReputation( String idEntity )
    {
        return ownerEntity.getLastReputation( idEntity );
    }

    /**
     * This method describes how to compute the reputation value for an entity. It must overriden by clients according to their needs.
     * @param context   context of the reputation update.
     * @param idTarget  unique identifier of the target entity.
     * @param idSource  unique identifier of the source entity.
     * @return  the reputation value.
     */
    @Override
    public T compute( String context, String idTarget, String idSource )
    {
        return null;
    }

}



