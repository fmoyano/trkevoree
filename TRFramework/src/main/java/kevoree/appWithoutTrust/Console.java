package kevoree.appWithoutTrust;

import kevoree.appUtilCommon.ConsolePanel;
import kevoree.appUtilCommon.TextTypedLocallyInterface;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.Port;
import org.kevoree.library.java.core.console.ConsoleFrame;
import org.kevoree.library.java.core.console.TabbedConsoleFrame;
import org.kevoree.log.Log;
import javax.swing.*;
import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * Offers a Graphical frame where input text is displayed and where text can also be typed in.
 * Created with IntelliJ IDEA.
 * User: gregory.nain
 * Date: 02/12/2013
 * Time: 10:47
 */
@ComponentType
public class Console implements TextTypedLocallyInterface {

    private ConsolePanel<Console> thisConsole;
    private ConsoleFrame standaloneFrame;

    @Param(defaultValue = "true")
    protected Boolean showInTab = true;

    @Output
    protected Port textEntered;

    @KevoreeInject
    protected Context cmpContext;

    long startTime, endTime;

    /**
     * Uniquely identifies the console in tabs panels. Also used as title for the Standalone frame.
     */
    private String consoleKey;

    @Start
    public void startConsole()
    {
        //Uniquely identifies the console
        consoleKey = cmpContext.getInstanceName() + "@" + cmpContext.getNodeName();
        thisConsole = new ConsolePanel( this );
        thisConsole.appendSystem("/***** CONSOLE READY ******/ ");

        if(showInTab) {
            TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    standaloneFrame = new ConsoleFrame(consoleKey);
                    standaloneFrame.init(thisConsole);
                }
            });
        }
    }

    @Stop
    public void stop()
    {
        if(showInTab) {
            TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
            Log.debug("Stopped from TAB");
        } else {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
            }
        }
        standaloneFrame = null;
        thisConsole = null;
        consoleKey = null;
    }


    @Update
    public void update()
    {
        if(showInTab) {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
                standaloneFrame = null;
                consoleKey = cmpContext.getInstanceName() + "@" + cmpContext.getNodeName();
                TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
            }
        } else {
            if(standaloneFrame == null) {
                TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
                consoleKey = cmpContext.getInstanceName() + "@" + cmpContext.getNodeName();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        standaloneFrame = new ConsoleFrame(consoleKey);
                        standaloneFrame.init(thisConsole);
                    }
                });
            }
        }
    }

    public void appendSystem(String text)
    {
        thisConsole.appendSystem(text);
    }

    public void textTypedLocally( String text )
    {
        //System.out.println( consoleKey + " sent text at " + new java.sql.Timestamp( Calendar.getInstance().getTime().getTime() ).toString() )
        long currentTime = System.nanoTime();
        textEntered.send( text + "." + currentTime );
    }

    @Input
    public void showText(Object text)
    {
        if( text != null ) {
            String msgReceived = text.toString();
            StringTokenizer st = new StringTokenizer( msgReceived, ".");
            String msg = st.nextToken();
            thisConsole.appendIncomming( "->" + msg );
            long startTime = Long.valueOf( st.nextToken() );
            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;
            System.out.println( "Time elapsed: " + timeElapsed );
            //System.out.println( consoleKey + " received text at " + new java.sql.Timestamp( Calendar.getInstance().getTime().getTime() ).toString() );

        }
    }
}
