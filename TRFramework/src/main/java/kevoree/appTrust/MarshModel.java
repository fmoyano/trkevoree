package kevoree.appTrust;

import kevoree.trustAPI.TrustException;
import kevoree.trustAPI.TrustModel;
import org.kevoree.annotation.ComponentType;
import org.kevoree.annotation.Start;

/**
 * Created by franciscomoyanolara on 08/01/15.
 */
@ComponentType
public class MarshModel extends TrustModel<String, Float>
{
    @Start
    public void start()
    {
        try {
            super.start();
        } catch (TrustException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String compute(String context, String idTrustee, String idTrustor)
    {
        float utility = Float.valueOf( getFactorValue( context, "utility", idTrustor ));
        float importance = Float.valueOf( getFactorValue(context, "importance", idTrustor));

        String generalTrustString = getFactorValue( context, "generalTrust", idTrustor, idTrustee );
        float generalTrust = 0.5f;
        if( generalTrustString != null )
        {
            generalTrust = Float.valueOf( getFactorValue( context, "generalTrust", idTrustor, idTrustee ));
            System.out.println("generalTrust retrieved with value " + generalTrust );
        }
        else
        {
            System.out.println("No information about past values of trust yet");
        }

        //Now, we're using the last situational trust value as the general trust
        //A more realistic approach:
        //- Factor that stores the accummulated trust
        //- Factor that stores the number of trust evaluations
        //- Calculate average: general trust =  accummulated trust / number of trust evaluations
        float situationalTrust = utility * importance * generalTrust;
        addFactor( context, "generalTrust", situationalTrust, idTrustor, idTrustee );

        return String.valueOf(situationalTrust);
    }

    @Override
    protected String computeThreshold(String context, String idTrustee, String idTrustor)
    {
        System.out.println("Threshold trying to access perceivedRisk...");
        float perceivedRisk = Float.valueOf( getFactorValue( context, "perceivedRisk", idTrustor ));
        System.out.println("perceivedRisk retrieved with value " + perceivedRisk );

        System.out.println("Threshold trying to access perceivedCompetence...");
        String perceivedCompetenceString = getFactorValue( context, "perceivedCompetence", idTrustor, idTrustee );
        float perceivedCompetence = 0.5f;
        if( perceivedCompetenceString != null )
        {
            perceivedCompetence = Float.valueOf( getFactorValue( context, "perceivedCompetence", idTrustor, idTrustee ));
            System.out.println("perceivedCompetence retrieved with value " + perceivedCompetence );
        }
        else
        {
            System.out.println("No information about past values of perceived competence yet");
        }

        System.out.println("Threshold trying to access generalTrust...");
        String generalTrustString = getFactorValue( context, "generalTrust", idTrustor, idTrustee );
        float generalTrust = 0.5f;
        if( generalTrustString != null )
        {
            generalTrust = Float.valueOf( getFactorValue( context, "generalTrust", idTrustor, idTrustee ));
            System.out.println("generalTrust retrieved with value " + generalTrust );
        }
        else
        {
            System.out.println("No information about past values of trust yet");
        }

        System.out.println("Threshold trying to access importance...");
        float importance = Float.valueOf( getFactorValue(context, "importance", idTrustor));
        System.out.println("importance retrieved with value " + importance );

        float threshold = importance * perceivedRisk / (perceivedCompetence + generalTrust );

        return String.valueOf( threshold );
    }

    @Override
    protected String incrementFactor(String currentValue, String increment) {
        return String.valueOf( Float.valueOf( currentValue ) + Float.valueOf( increment ));
    }
}
