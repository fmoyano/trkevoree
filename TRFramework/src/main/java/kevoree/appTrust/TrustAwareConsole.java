package kevoree.appTrust;

import kevoree.appUtilCommon.ConsolePanel;
import kevoree.appUtilCommon.TextTypedLocallyInterface;
import kevoree.reputationAPI.CentralKevReputableEntity;
import kevoree.trustAPI.TrustEntity;
import kevoree.trustAPI.TrustException;
import org.kevoree.annotation.*;
import org.kevoree.api.Context;
import org.kevoree.api.Port;
import org.kevoree.library.java.core.console.ConsoleFrame;
import org.kevoree.library.java.core.console.TabbedConsoleFrame;
import org.kevoree.log.Log;

import javax.swing.*;
import java.util.*;

/**
 * Offers a Graphical frame where input text is displayed and where text can also be typed in.
 * Created with IntelliJ IDEA.
 * User: gregory.nain
 * Date: 02/12/2013
 * Time: 10:47
 */
@ComponentType
public class TrustAwareConsole extends TrustEntity<String,Float> implements TextTypedLocallyInterface
{

    private ConsolePanel<TrustAwareConsole> thisConsole;
    private ConsoleFrame standaloneFrame;

    @Param(defaultValue = "true")
    protected Boolean showInTab = true;

    @Output
    protected Port textEntered;

    @KevoreeInject
    protected Context cmpContext;

    //Buffer to store the last message prior to showing it; we have to check
    //if we trust the sender first
    private Map<String,String> lastMessageReceived;

    /**
     * Uniquely identifies the console in tabs panels. Also used as title for the Standalone frame.
     */
    private String consoleKey;

    @Start
    public void startConsole()
    {
        super.start();
        //Uniquely identifies the console
        consoleKey = getUID();
        thisConsole = new ConsolePanel( this );
        thisConsole.appendSystem("/***** CONSOLE READY ******/ ");

        if(showInTab) {
            TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    standaloneFrame = new ConsoleFrame(consoleKey);
                    standaloneFrame.init(thisConsole);
                }
            });
        }
        lastMessageReceived = new HashMap();
    }

    @Stop
    public void stop()
    {
        if(showInTab) {
            TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
            Log.debug("Stopped from TAB");
        } else {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
            }
        }
        standaloneFrame = null;
        thisConsole = null;
        consoleKey = null;
    }


    @Update
    public void update()
    {
        super.update();
        if(showInTab) {
            if(standaloneFrame != null) {
                standaloneFrame.dispose();
                standaloneFrame = null;
                consoleKey = getUID();
                TabbedConsoleFrame.getInstance().addTab(thisConsole, consoleKey);
            }
        } else {
            if(standaloneFrame == null) {
                TabbedConsoleFrame.getInstance().releaseTab(consoleKey);
                consoleKey = getUID();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        standaloneFrame = new ConsoleFrame(consoleKey);
                        standaloneFrame.init(thisConsole);
                    }
                });
            }
        }
    }

    public void appendSystem( String text )
    {
        thisConsole.appendSystem(text);
    }

    public void textTypedLocally( String text )
    {
        long currentTime = System.nanoTime();
        System.out.println("Current time " + currentTime );
        textEntered.send( getUID() + "."
                + getBelongingGroup()  + "."
                + currentTime  + "."
                + text );
    }

    private long lastTime = 0;

    @Input
    public void showText(Object text)
    {
        if( text != null ) {
            String msg = text.toString();
            StringTokenizer st = new StringTokenizer( msg, "." );
            String idTarget = st.nextToken();
            String group = st.nextToken();
            String message = st.nextToken() + "." + st.nextToken();
            lastMessageReceived.put( idTarget, message );

            if( message.equals( "shit\n") || message.equals("fuck\n") )
            {
                //Decrease in -0.1 the factor perceivedCompetence about the target idTarget
                //If the factor does not exist, initialize it with 0.5f
                changeSubjectiveFactor("perceivedCompetence", -0.1f, 0.5f, idTarget );
            }
            try {
                requestTrustUpdate( idTarget, false );
            } catch (TrustException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void trustRelationshipUpdated(final String uidTarget, List<String> newVal) {

        float trustValue = Float.valueOf(newVal.get(0));
        float threshold = Float.valueOf(newVal.get(1));

        StringTokenizer st = new StringTokenizer( lastMessageReceived.get( uidTarget ), ".");
        long timeSent = Long.valueOf(st.nextToken());
        long timeReceived = System.nanoTime();
        long timeElapsed = timeReceived - timeSent;

        System.out.println( "Time sent: " + timeSent );
        System.out.println( "Time received: " + timeReceived );
        System.out.println( "Time elapsed: " + timeElapsed );

        if( trustValue >= threshold )
        {
            thisConsole.appendIncomming( st.nextToken() );
        }
        else
        {
            System.out.println( getUID() + " does not trust " + uidTarget );
        }
    }

    @Override
    protected List<String> convertBackToValuesTypes( List<String> vals )
    {
        //No need for conversion from String to String
        return vals;
    }
}
