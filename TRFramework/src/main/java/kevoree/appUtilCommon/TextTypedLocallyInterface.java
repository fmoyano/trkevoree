package kevoree.appUtilCommon;

/**
 * Created by franciscomoyanolara on 26/11/14.
 */
public interface TextTypedLocallyInterface {

    public void textTypedLocally(String text);
}
