Para poder generar el código del .ecore, tuve que cambiar lo siguiente:
- Añadir org.kevoree.modeling.microframework versión 3.5.12
- Usar kotlin-stdlib con versión 0.8.11
- Usar plugin org.kevoree.moeling.kotlin.generator.mavenplugin versión 3.5.12
- Cambiar versión de kotlin-maven-plugin a la versión 0.8.11
