# README #

This is a trust and reputation framework that is implemented on top of Kevoree (http://kevoree.org), an open-source self-adaptive platform that implements the models@run.time paradigm. 

This framework allows developers to implement trust models in Kevoree applications and to specify reconfiguration policies based on trust and reputation information. This allows applications to  make reconfiguration decisions at runtime based on trust relationships among components and on their reputation scores. 

The actual framework sources are in the TRFramework directory. ReputationModel and TrustModel directories are Maven projects that I used to generate sources for the metamodel management from Eclipse Modelling Framework (EMF) ecore models. These sources are used by the actual framework to manage the trust and reputation models implemented by developers. TRFramework_old directory contains an older version of the framework and is only kept for longing issues :)

This work is part of my ph.D., in which I proposed several methodologies and tools to help software engineers in integrating trust and reputation along the software development life cycle. 

### Who do I talk to? ###

For any questions, contact me in the following e-mail: fran[dot]uma[at]gmail[dot]com